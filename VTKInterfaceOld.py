# David Mashburn
# 3/17/2008

import numpy as n
import vtk
import MyVTKColormaps
#import math

simpleLut=MyVTKColormaps.MyLut(MyVTKColormaps.MyVtkMaps.jet,-10,10)

class VTKPlotObject:
    def __init__(self,vtkCmap=None,use2D=True,use3D=True): # Always maintain your own lookup table...reset it after init...
        if vtkCmap==None: # 
            vtkCmap=MyVTKColormaps.MyVtkMaps.jet
        
        self.lut=MyVTKColormaps.MyLut(vtkCmap,-10,10)
        myVTKLT=self.lut.vtkLT
        [self.NX,self.NY]=[0,0]
        self.use2D=use2D
        self.use3D=use3D
        
        self.vArr=vtk.vtkFloatArray()
        self.imgdat = vtk.vtkImageData()
        self.res=vtk.vtkImageReslice()
        self.res.SetInput(self.imgdat)
        self.shrinkFactor = 1
        self.shrink = vtk.vtkImageShrink3D()
        self.shrink.SetShrinkFactors(self.shrinkFactor, self.shrinkFactor, 1)
        self.shrink.AveragingOn()
        self.shrink.SetInputConnection(self.res.GetOutputPort())
        
        self.cmap=vtk.vtkImageMapToColors()
        self.cmap.SetInputConnection(self.shrink.GetOutputPort())
        self.cmap.SetOutputFormatToRGB()
        
        if self.use2D:
            # specifically for image
            self.imview=vtk.vtkImageViewer()
            self.imview.SetInputConnection(self.cmap.GetOutputPort())
        
        if self.use3D:
            # specifically for surface:
            self.geometry = vtk.vtkImageDataGeometryFilter()
            self.geometry.SetInputConnection(self.shrink.GetOutputPort())
            
            self.warp = vtk.vtkWarpScalar()
            self.warp.SetInputConnection(self.geometry.GetOutputPort())
            self.warp.UseNormalOn()
            self.warp.SetNormal(0,0,1)
            
            self.merge = vtk.vtkMergeFilter()
            self.merge.SetGeometry(self.warp.GetOutput())
            self.merge.SetScalars(self.cmap.GetOutput())
            
            self.mapper = vtk.vtkDataSetMapper()
            self.mapper.SetInputConnection(self.merge.GetOutputPort())
            self.mapper.SetScalarRange(0, 255)
            self.mapper.ImmediateModeRenderingOff()
            self.actor = vtk.vtkActor()
            self.actor.SetMapper(self.mapper)
            
            self.ren = vtk.vtkRenderer()
            self.ren.AddActor(self.actor)
            self.renWin = vtk.vtkRenderWindow()
            self.renWin.AddRenderer(self.ren)
            self.iren = vtk.vtkRenderWindowInteractor()
            self.iren.SetRenderWindow(self.renWin)
        
        self.SetColormap(myVTKLT)
        
    def SetNumpyArray(self,dat,scale=[1,1,1],warpFactor=10, center=True):
        self.inputArr=dat
        
        if(dat.dtype.char=='c'):
            self.vArr=vtk.vtkCharArray()
        elif(dat.dtype.char=='b'):
            self.vArr=vtk.vtkCharArray()
        elif(dat.dtype.char=='h'):
            self.vArr=vtk.vtkShortArray()
        elif(dat.dtype.char=='l'):
            self.vArr=vtk.vtkLongArray()
        elif(dat.dtype.char=='B'):
            self.vArr=vtk.vtkUnsignedCharArray()
        elif(dat.dtype.char=='H'):
            self.vArr=vtk.vtkUnsignedShortArray()
        elif(dat.dtype.char=='L'):
            self.vArr=vtk.vtkUnsignedLongArray()
        elif(dat.dtype.char=='f'):
            self.vArr=vtk.vtkFloatArray()
        elif(dat.dtype.char=='d'):
            self.vArr=vtk.vtkDoubleArray()
            self.imgdat.SetScalarTypeToDouble()
        else:
            print("Unknown Bytecode Passed to VTKInterface!!!")
            self.vArr=self.vArr=vtk.vtkFloatArray()
        
        [self.NX,self.NY]=self.inputArr.shape
        self.vArr.SetVoidArray(self.inputArr,self.inputArr.size,1)
        self.imgdat.GetPointData().SetScalars(self.vArr)
        self.imgdat.SetDimensions(self.NY, self.NX, 1) #set dimensions as necessary
        
        if(dat.dtype.char=='c'):
            self.imgdat.SetScalarTypeToChar()
        elif(dat.dtype.char=='b'):
            self.imgdat.SetScalarTypeToChar()
        elif(dat.dtype.char=='h'):
            self.imgdat.SetScalarTypeToShort()
        elif(dat.dtype.char=='l'):
            self.imgdat.SetScalarTypeToLong()
        elif(dat.dtype.char=='B'):
            self.imgdat.SetScalarTypeToUnsignedChar()
        elif(dat.dtype.char=='H'):
            self.imgdat.SetScalarTypeToUnsignedShort()
        elif(dat.dtype.char=='L'):
            self.imgdat.SetScalarTypeToUnsignedLong()
        elif(dat.dtype.char=='f'):
            self.imgdat.SetScalarTypeToFloat()
        elif(dat.dtype.char=='d'):
            pass
        else:
            print("Unknown Bytecode Passed to VTKInterface!!!")
            self.imgdat.SetScalarTypeToDouble()
        
        
        
        if(center==True):
            self.imgdat.SetOrigin(0.5*self.NY,0.5*self.NX,0)
        else:
            self.imgdat.SetOrigin(0,0,0)
            
        self.imgdat.SetSpacing(1, 1, 1) #set spacing as necessary
        if self.use3D:
            self.warp.SetScaleFactor(warpFactor)
        self.res.SetResliceAxesDirectionCosines((scale[0],0,0),(0,scale[1],0),(0,0,scale[2]))
        #self.renWin. fix window size...
    def ImShow(self,dat,scale=[1,1,1],warpFactor=10, center=True):
        if not self.use2D:
            print 'Must use 2D for ImShow'
            return
        self.SetNumpyArray(dat,scale,warpFactor,center)
        self.imview.Render()
    def Surf(self,dat,scale=[1,1,1],warpFactor=10, center=True,resetCamera=True):
        if not self.use3D:
            print 'Must use 3D for Surf and SurfStart'
            return
        self.SetNumpyArray(dat,scale,warpFactor,center)
        self.renWin.Render()
        if(resetCamera):
            self.ren.ResetCamera()
    def SurfStart(self):
        if not self.use3D:
            print 'Must use 3D for Surf and SurfStart'
            return
        self.renWin.Render()
        self.ren.ResetCamera()
        self.iren.Initialize()
        self.iren.Start()
    def SetColormap(self,myVTKLT):
        self.cmap.SetLookupTable(myVTKLT)
        if self.use2D:
            self.imview.SetColorWindow(255.0)
            self.imview.SetColorLevel(127.5)
    def SetShrinkFactor(self,factor):
        self.shrinkFactor=factor
        self.shrink.SetShrinkFactors(self.shrinkFactor, self.shrinkFactor, 1)
    def SaveImage(self,fileName="out.jpg",plotType='image'):
        w2if=vtk.vtkWindowToImageFilter()
        
        if self.use2D:
            if self.use3D==False or plotType=='image':
                w2if.SetInput(self.imview.GetRenderWindow())
        if self.use3D:
            if self.use3D==False or plotType=='surf':
                w2if.SetInput(self.renWin)
            
        writer=vtk.vtkJPEGWriter()
        writer.SetInput(w2if.GetOutput())
        writer.SetQuality(100)
        writer.SetFileName(fileName)
        writer.ProgressiveOff()
        writer.Write()
##        vtkWindowToImageFilter *w2if = vtkWindowToImageFilter::New();
##        w2if->SetInput( renWin);
##        vtkJPEGWriter *writeJPEG = vtkJPEGWriter::New();
##        writeJPEG->SetInput (w2if->GetOutput() );
##        writeJPEG->SetQuality(100);
##        writeJPEG->SetFileName("out.jpg");
##        writeJPEG->ProgressiveOff();
##        writeJPEG->Write();
##        writeJPEG->Delete();

if __name__=="__main__":
    #Arr=10*n.array([[1,2,3,4]*10,[2,3,4,5]*10,[3,4,5,6]*10,[4,5,6,7]*10,[7,8,9,10]*10]*10,dtype=n.float32)-100
    #Arr=10*n.array([[1,2,3,4],[2,3,4,5],[3,4,5,6],[4,5,6,7],[7,8,9,10]],dtype=n.float32)
    #Arr=n.array([[-85.5,-85.5]*120,[20,20]*120]*300,dtype=n.float32)
    Arr=n.array([-85.5*n.ones([300,800],dtype=n.float32),20*n.ones([300,800],dtype=n.float32)],dtype=n.float32)
    Arr=Arr.reshape(600,800)
    myArr=n.array(Arr,dtype=n.float32)
    
    lut=MyVTKColormaps.MyLut(MyVTKColormaps.MyVtkMaps.flipjet,-10,10)
    
    vI=VTKPlotObject()#lut.vtkLT)
    vI.Surf(myArr)
    
    #vI.SurfStart()
    
    import time
    c=time.clock()
    
    x=n.arange(200)
    y=n.arange(200)
    myArr=n.array(0/10.*n.outer(n.sin(x*3*n.pi/100),n.cos(y*3*n.pi/100)),n.float32)
    vI.ImShow(myArr)
    vI.Surf(myArr,resetCamera=False)
    vI.SurfStart()
    
    import wx
    app=wx.App(0);app.MainLoop()
    w=wx.MessageDialog(None,'''Close this window,
adjust the surf plot with the mouse,
then push the close button on the VTK surf window to continue...''')
    w.Show()
    vI.SetShrinkFactor(1)
    for g in range(0,100,10):
        #a=n.array([-85.5*n.ones([200,240],dtype=n.float32),20*n.ones([200,240],dtype=n.float32),20*n.ones([200,240],dtype=n.float32)],dtype=n.float32)
        #myArr=n.ones([550-g,int(220-g/2.5)],n.float32)
        #myArr[300:(300+g),:]=g
        #myArr[0:20]=-20
        x=n.arange(200)
        y=n.arange(200)
        myArr=n.array(g/10.*n.outer(n.sin(x*3*n.pi/100),n.cos(y*3*n.pi/100)),n.float32)
        vI.ImShow(myArr)
        vI.Surf(myArr,resetCamera=False)
        vI.SaveImage('C:\\Users\\mashbudn\\Desktop\\outI.jpg','image')
        vI.SaveImage('C:\\Users\\mashbudn\\Desktop\\outS.jpg','surf')
    
    print time.clock()-c
    
    vI.SurfStart()
    wx.MessageBox('''Close this window to end the program''')

#All in one command to use this module
#import numpy as n;import VTKInterface as VI;vI=VI.VTKPlotObject();vI.Surf(n.r_[[[0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,1,0,0,0,0,0],[0,0,0,0,1,2,1,0,0,0,0],[0,0,0,1,2,3,2,1,0,0,0],[0,0,0,0,1,2,1,0,0,0,0],[0,0,0,0,0,1,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0]]]**(1./2)*2,warpFactor=1);vI.SurfStart()
#This one does a gaussian (requires scipy)
#import numpy as n;import VTKInterface as VI;from scipy import ndimage;a=n.zeros([10,10]);a[5,5]=100;b=ndimage.gaussian_filter(a,2);vI=VI.VTKPlotObject();vI.Surf(b,warpFactor=1);vI.SurfStart()
