import numpy as np

def diced_array(arr, ndivisions, depth=0):
    '''Takes an ND array and returns it cut up into pieces
    
    Inputs:
        arr: array
        ndivisions: how many slices to split each dimension into
                    (ones are assumed for deeper dimensions if not enough
                     arguments are passed)
    Returns:
        a nested list of sub-arrays of arr
    
    Uses array_slice to avoid dropping any elements, but this means that
    without external assurances, the resulting arrays may have different sizes
    This does skew towards making the lower index partitions (i.e. top/left for images)
    slightly larger than the higher ones
    '''
    arr = np.asanyarray(arr)
    
    diced = np.array_split(arr, ndivisions[0], axis=depth)
    if len(ndivisions) == 1 or depth + 1 >= arr.ndim:
        return diced
    else:
        return [diced_array(i, ndivisions[1:], depth=depth + 1)
                for i in diced]

if __name__ == '__main__':
    a = np.arange(24).reshape(6, 4)
    print diced_array(a, [4,2])
