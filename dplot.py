import matplotlib.pyplot as plt

from np_utils import flatten,intersperse

def _dplot_base(plotFunction,coordinatePairs,*args,**kwds):
    x,y = zip(*coordinatePairs)
    if 'flipXY' in kwds:
        flipXY = kwds['flipXY']
        del(kwds['flipXY'])
        if flipXY==True:
            x,y = y,x
    return plotFunction(x,y,*args,**kwds)

def dplot(coordinatePairs,*args,**kwds):
    '''A wrapper around matplotlib.pyplot.plot that takes
       coordinate pairs... Why didn't I do this sooner?!?!?
       Extra kwd argument for "flipXY". If True, swap x and y.'''
    return _dplot_base(plt.plot,coordinatePairs,*args,**kwds)

def dfill(coordinatePairs,*args,**kwds):
    '''A wrapper around matplotlib.pyplot.fill that takes
       coordinate pairs... Why didn't I do this sooner?!?!?
       Extra kwd argument for "flipXY". If True, swap x and y.'''
    return _dplot_base(plt.fill,coordinatePairs,*args,**kwds)

dplot.__doc__ = dplot.__doc__+'\n\n'+plt.plot.__doc__
dfill.__doc__ = dfill.__doc__+'\n\n'+plt.fill.__doc__

def nonefyLines(lineList):
    '''Take a list of lines in [ [[x1,y1],[x2,y2]], ... ] format and
       convert them to a format suitable for fast plotting with dplot
       by placing all points in a single flattened list with None's
       separating disconnected values, aka:
       [ [x1,y1],[x2,y2], None ,... ]'''
    return flatten(intersperse(lineList,[[None,None]]))

def nonefyAndOffsetLineArray(lineArr):
    '''Like nonefyLines, but takes an array and offsets x&y by 0.5
       Suitable for plotting lines on top of an image
       (See nonefyLines for more details)'''
    return nonefyLines((lineArr-0.5).tolist())

def dplotLines(lineList):
    return dplot(nonefyLines(lineList))

colorDict = {'r':(1,0,0,0.5),'g':(0,1,0,0.5),'b':(0,0,1,0.5),'c':(0,1,1,0.5),'m':(1,0,1,0.5),'y':(1,1,0,0.5),'k':(0,0,0,0.5)}

def errorPlot(timeAxis,mean,err,color,label,plotOuterLines=False,dashes=None,**kwds):
    ''''A combination of dplot and dfill to make an error-bounds plot with half-opacity flange'''
    fillColor = ( colorDict[color] if colorDict.has_key(color) else color )
    plt.fill_between(timeAxis,mean-err,mean+err,color=fillColor)
    plt.plot(timeAxis,mean,color,label=label,**kwds)
    if plotOuterLines:
        if dashes!=None:
            kwds['dashes'] = dashes
        plt.plot(timeAxis,mean+err,color+'--',label=label,**kwds)
        plt.plot(timeAxis,mean-err,color+'--',label=label,**kwds)

def circle(center,radius,*args,**kwds):
    '''A wrapper around plt.gca().add_artist(plt.Circle(center,radius,*args,**kwds))'''
    c = plt.Circle(center,radius,*args,**kwds)
    return plt.gca().add_artist(c)

def xylim((xmin,xmax),(ymin,ymax)):
    plt.xlim(xmin,xmax)
    plt.ylim(ymin,ymax)
