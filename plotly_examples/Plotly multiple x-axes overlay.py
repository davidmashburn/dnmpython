#!/usr/bin/env python
# coding: utf-8

# In[1]:


import plotly
import plotly.graph_objects as go


# In[2]:


fig = go.Figure(
    data=[
        go.Scatter(
            x=[1, 2, 3],
            y=[4, 5, 6],
            name="xaxis1 data",
            xaxis="x1",
        ),
        go.Scatter(
            x=[2, 3, 4],
            y=[40, 50, 60],
            name="xaxis1 data (B)",
            xaxis="x1",
        ),
        go.Scatter(
            x=[],
            y=[],
            xaxis="x2",
        ),
        
    ],
    layout=dict(
        xaxis1=dict(
            range=[0, 5],
            title="raw score",
        ),
        xaxis2=dict(
            tickvals=[1, 2.5, 4],
            ticktext=["<- Easy", "Medium", "Hard ->"],
            titlefont=dict(
                color="#ff7f0e"
            ),
            tickfont=dict(
                color="#ff7f0e"
            ),
            anchor="free",
            overlaying="x1",
            side="bottom",
            position=0.15,
            
        ),
        yaxis=dict(
            title="yaxis title",
        ),
        title_text="multiple x-axes example",
        width=800,
    ),
)

fig.show()


# In[ ]:




