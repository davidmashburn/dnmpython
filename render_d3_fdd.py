import os
import glob
import json
import time
from collections import OrderedDict

templ = '''<!DOCTYPE html>
<meta charset="utf-8">
<input id="SaveSVG" type="button" value="Save SVG" onclick="save_svg();" />
<br>
<svg width="${canvasw}" height="${canvash}"></svg>
<script src="https://d3js.org/d3.v4.min.js"></script>
<script>

var svg = d3.select("svg"),
    width = +svg.attr("width"),
    height = +svg.attr("height");

var color = d3.scaleOrdinal(d3.schemeCategory20);

var simulation = d3.forceSimulation()
    .force("link", d3.forceLink().id(d => d.id)
                     .distance(20*${force_scale}))
    .force("charge", d3.forceManyBody().strength(-30*${force_scale}))
    .force("center", d3.forceCenter(width / 2, height / 2));

var graph = ${graph};

var link = svg.append("g")
    .attr("class", "links")
    .selectAll("line")
    .data(graph.links)
    .enter().append("line")
    .attr("stroke-width", d => ${scale}*Math.sqrt(d.value))
    .attr("stroke", "#999")
    .attr("stroke-opacity", "0.6");
    

var node = svg.append("g")
    .attr("class", "nodes")
    .selectAll("circle")
    .data(graph.nodes)
    .enter().append("circle")
    .attr("r", ${scale}*5)
    .attr("fill", d => color(d.group))
    .attr("stroke", "#fff")
    .attr("stroke-width", String(1.5*${scale})+"px")
    .call(d3.drag().on("start", dragstarted)
                   .on("drag", dragged)
                   .on("end", dragended))
    .on('click', click);

node.append("title")
    .text(d => d.id);

simulation.nodes(graph.nodes)
          .on("tick", ticked);

simulation.force("link")
          .links(graph.links);

save_counter = 0;

function ticked() {
  link.attr("x1", d => d.source.x)
      .attr("y1", d => d.source.y)
      .attr("x2", d => d.target.x)
      .attr("y2", d => d.target.y);
  node.attr("cx", d => d.x)
      .attr("cy", d => d.y);
  
  if(${save_freq} && save_counter % ${save_freq} == 0) {
    save_svg();
    console.log('SAVE ' + save_counter)
  };
  save_counter++;
};

function dragstarted(d) {
  if (!d3.event.active) simulation.alphaTarget(0.3).restart();
  d.fx = d.x;
  d.fy = d.y;
}

function dragged(d) {
  d.fx = d3.event.x;
  d.fy = d3.event.y;
}

function dragended(d) {
  if (!d3.event.active) simulation.alphaTarget(0);
  d.fx = null;
  d.fy = null;
}

${click_function}

function save_svg() {
  var html = d3.select("svg")
               .attr("title", "test2")
               .attr("version", 1.1)
               .attr("xmlns", "http://www.w3.org/2000/svg")
               .node().outerHTML;
  var svgData = html.split(">").join(">\\n"); //hack
  var svgBlob = new Blob([svgData], {type:"image/svg+xml;charset=utf-8"});
  var svgUrl = URL.createObjectURL(svgBlob);
  var downloadLink = document.createElement("a");
  downloadLink.href = svgUrl;
  downloadLink.download = "newesttree.svg";
  document.body.appendChild(downloadLink);
  downloadLink.click();
  document.body.removeChild(downloadLink);
}

</script>
'''

click_default = '''function click(d) {
  if (d3.event.defaultPrevented) return;
  console.log('clicked');
}
'''

sample_nodes = [
  ("Myriel", 1),
  ("Napoleon", 1),
  ("Mlle.Baptistine", 1),
  ("Mme.Magloire", 1),
  ("CountessdeLo", 1),
  ("Geborand", 1),
  ("Champtercier", 1),
  ("Cravatte", 1),
  ("Count", 1),
  ("OldMan", 1),
  ("Labarre", 2),
  ("Valjean", 2),
  ("Marguerite", 3),
  ("Mme.deR", 2),
  ("Isabeau", 2),
  ("Gervais", 2),
  ("Tholomyes", 3),
  ("Listolier", 3),
  ("Fameuil", 3),
  ("Blacheville", 3),
  ("Favourite", 3),
  ("Dahlia", 3),
  ("Zephine", 3),
  ("Fantine", 3),
  ("Mme.Thenardier", 4),
  ("Thenardier", 4),
  ("Cosette", 5),
  ("Javert", 4),
  ("Fauchelevent", 0),
  ("Bamatabois", 2),
  ("Perpetue", 3),
  ("Simplice", 2),
  ("Scaufflaire", 2),
  ("Woman1", 2),
  ("Judge", 2),
  ("Champmathieu", 2),
  ("Brevet", 2),
  ("Chenildieu", 2),
  ("Cochepaille", 2),
  ("Pontmercy", 4),
  ("Boulatruelle", 6),
  ("Eponine", 4),
  ("Anzelma", 4),
  ("Woman2", 5),
  ("MotherInnocent", 0),
  ("Gribier", 0),
  ("Jondrette", 7),
  ("Mme.Burgon", 7),
  ("Gavroche", 8),
  ("Gillenormand", 5),
  ("Magnon", 5),
  ("Mlle.Gillenormand", 5),
  ("Mme.Pontmercy", 5),
  ("Mlle.Vaubois", 5),
  ("Lt.Gillenormand", 5),
  ("Marius", 8),
  ("BaronessT", 5),
  ("Mabeuf", 8),
  ("Enjolras", 8),
  ("Combeferre", 8),
  ("Prouvaire", 8),
  ("Feuilly", 8),
  ("Courfeyrac", 8),
  ("Bahorel", 8),
  ("Bossuet", 8),
  ("Joly", 8),
  ("Grantaire", 8),
  ("MotherPlutarch", 9),
  ("Gueulemer", 4),
  ("Babet", 4),
  ("Claquesous", 4),
  ("Montparnasse", 4),
  ("Toussaint", 5),
  ("Child1", 10),
  ("Child2", 10),
  ("Brujon", 4),
  ("Mme.Hucheloup", 8),
]
sample_links = [
  ("Napoleon", "Myriel", 1),
  ("Mlle.Baptistine", "Myriel", 8),
  ("Mme.Magloire", "Myriel", 10),
  ("Mme.Magloire", "Mlle.Baptistine", 6),
  ("CountessdeLo", "Myriel", 1),
  ("Geborand", "Myriel", 1),
  ("Champtercier", "Myriel", 1),
  ("Cravatte", "Myriel", 1),
  ("Count", "Myriel", 2),
  ("OldMan", "Myriel", 1),
  ("Valjean", "Labarre", 1),
  ("Valjean", "Mme.Magloire", 3),
  ("Valjean", "Mlle.Baptistine", 3),
  ("Valjean", "Myriel", 5),
  ("Marguerite", "Valjean", 1),
  ("Mme.deR", "Valjean", 1),
  ("Isabeau", "Valjean", 1),
  ("Gervais", "Valjean", 1),
  ("Listolier", "Tholomyes", 4),
  ("Fameuil", "Tholomyes", 4),
  ("Fameuil", "Listolier", 4),
  ("Blacheville", "Tholomyes", 4),
  ("Blacheville", "Listolier", 4),
  ("Blacheville", "Fameuil", 4),
  ("Favourite", "Tholomyes", 3),
  ("Favourite", "Listolier", 3),
  ("Favourite", "Fameuil", 3),
  ("Favourite", "Blacheville", 4),
  ("Dahlia", "Tholomyes", 3),
  ("Dahlia", "Listolier", 3),
  ("Dahlia", "Fameuil", 3),
  ("Dahlia", "Blacheville", 3),
  ("Dahlia", "Favourite", 5),
  ("Zephine", "Tholomyes", 3),
  ("Zephine", "Listolier", 3),
  ("Zephine", "Fameuil", 3),
  ("Zephine", "Blacheville", 3),
  ("Zephine", "Favourite", 4),
  ("Zephine", "Dahlia", 4),
  ("Fantine", "Tholomyes", 3),
  ("Fantine", "Listolier", 3),
  ("Fantine", "Fameuil", 3),
  ("Fantine", "Blacheville", 3),
  ("Fantine", "Favourite", 4),
  ("Fantine", "Dahlia", 4),
  ("Fantine", "Zephine", 4),
  ("Fantine", "Marguerite", 2),
  ("Fantine", "Valjean", 9),
  ("Mme.Thenardier", "Fantine", 2),
  ("Mme.Thenardier", "Valjean", 7),
  ("Thenardier", "Mme.Thenardier", 13),
  ("Thenardier", "Fantine", 1),
  ("Thenardier", "Valjean", 12),
  ("Cosette", "Mme.Thenardier", 4),
  ("Cosette", "Valjean", 31),
  ("Cosette", "Tholomyes", 1),
  ("Cosette", "Thenardier", 1),
  ("Javert", "Valjean", 17),
  ("Javert", "Fantine", 5),
  ("Javert", "Thenardier", 5),
  ("Javert", "Mme.Thenardier", 1),
  ("Javert", "Cosette", 1),
  ("Fauchelevent", "Valjean", 8),
  ("Fauchelevent", "Javert", 1),
  ("Bamatabois", "Fantine", 1),
  ("Bamatabois", "Javert", 1),
  ("Bamatabois", "Valjean", 2),
  ("Perpetue", "Fantine", 1),
  ("Simplice", "Perpetue", 2),
  ("Simplice", "Valjean", 3),
  ("Simplice", "Fantine", 2),
  ("Simplice", "Javert", 1),
  ("Scaufflaire", "Valjean", 1),
  ("Woman1", "Valjean", 2),
  ("Woman1", "Javert", 1),
  ("Judge", "Valjean", 3),
  ("Judge", "Bamatabois", 2),
  ("Champmathieu", "Valjean", 3),
  ("Champmathieu", "Judge", 3),
  ("Champmathieu", "Bamatabois", 2),
  ("Brevet", "Judge", 2),
  ("Brevet", "Champmathieu", 2),
  ("Brevet", "Valjean", 2),
  ("Brevet", "Bamatabois", 1),
  ("Chenildieu", "Judge", 2),
  ("Chenildieu", "Champmathieu", 2),
  ("Chenildieu", "Brevet", 2),
  ("Chenildieu", "Valjean", 2),
  ("Chenildieu", "Bamatabois", 1),
  ("Cochepaille", "Judge", 2),
  ("Cochepaille", "Champmathieu", 2),
  ("Cochepaille", "Brevet", 2),
  ("Cochepaille", "Chenildieu", 2),
  ("Cochepaille", "Valjean", 2),
  ("Cochepaille", "Bamatabois", 1),
  ("Pontmercy", "Thenardier", 1),
  ("Boulatruelle", "Thenardier", 1),
  ("Eponine", "Mme.Thenardier", 2),
  ("Eponine", "Thenardier", 3),
  ("Anzelma", "Eponine", 2),
  ("Anzelma", "Thenardier", 2),
  ("Anzelma", "Mme.Thenardier", 1),
  ("Woman2", "Valjean", 3),
  ("Woman2", "Cosette", 1),
  ("Woman2", "Javert", 1),
  ("MotherInnocent", "Fauchelevent", 3),
  ("MotherInnocent", "Valjean", 1),
  ("Gribier", "Fauchelevent", 2),
  ("Mme.Burgon", "Jondrette", 1),
  ("Gavroche", "Mme.Burgon", 2),
  ("Gavroche", "Thenardier", 1),
  ("Gavroche", "Javert", 1),
  ("Gavroche", "Valjean", 1),
  ("Gillenormand", "Cosette", 3),
  ("Gillenormand", "Valjean", 2),
  ("Magnon", "Gillenormand", 1),
  ("Magnon", "Mme.Thenardier", 1),
  ("Mlle.Gillenormand", "Gillenormand", 9),
  ("Mlle.Gillenormand", "Cosette", 2),
  ("Mlle.Gillenormand", "Valjean", 2),
  ("Mme.Pontmercy", "Mlle.Gillenormand", 1),
  ("Mme.Pontmercy", "Pontmercy", 1),
  ("Mlle.Vaubois", "Mlle.Gillenormand", 1),
  ("Lt.Gillenormand", "Mlle.Gillenormand", 2),
  ("Lt.Gillenormand", "Gillenormand", 1),
  ("Lt.Gillenormand", "Cosette", 1),
  ("Marius", "Mlle.Gillenormand", 6),
  ("Marius", "Gillenormand", 12),
  ("Marius", "Pontmercy", 1),
  ("Marius", "Lt.Gillenormand", 1),
  ("Marius", "Cosette", 21),
  ("Marius", "Valjean", 19),
  ("Marius", "Tholomyes", 1),
  ("Marius", "Thenardier", 2),
  ("Marius", "Eponine", 5),
  ("Marius", "Gavroche", 4),
  ("BaronessT", "Gillenormand", 1),
  ("BaronessT", "Marius", 1),
  ("Mabeuf", "Marius", 1),
  ("Mabeuf", "Eponine", 1),
  ("Mabeuf", "Gavroche", 1),
  ("Enjolras", "Marius", 7),
  ("Enjolras", "Gavroche", 7),
  ("Enjolras", "Javert", 6),
  ("Enjolras", "Mabeuf", 1),
  ("Enjolras", "Valjean", 4),
  ("Combeferre", "Enjolras", 15),
  ("Combeferre", "Marius", 5),
  ("Combeferre", "Gavroche", 6),
  ("Combeferre", "Mabeuf", 2),
  ("Prouvaire", "Gavroche", 1),
  ("Prouvaire", "Enjolras", 4),
  ("Prouvaire", "Combeferre", 2),
  ("Feuilly", "Gavroche", 2),
  ("Feuilly", "Enjolras", 6),
  ("Feuilly", "Prouvaire", 2),
  ("Feuilly", "Combeferre", 5),
  ("Feuilly", "Mabeuf", 1),
  ("Feuilly", "Marius", 1),
  ("Courfeyrac", "Marius", 9),
  ("Courfeyrac", "Enjolras", 17),
  ("Courfeyrac", "Combeferre", 13),
  ("Courfeyrac", "Gavroche", 7),
  ("Courfeyrac", "Mabeuf", 2),
  ("Courfeyrac", "Eponine", 1),
  ("Courfeyrac", "Feuilly", 6),
  ("Courfeyrac", "Prouvaire", 3),
  ("Bahorel", "Combeferre", 5),
  ("Bahorel", "Gavroche", 5),
  ("Bahorel", "Courfeyrac", 6),
  ("Bahorel", "Mabeuf", 2),
  ("Bahorel", "Enjolras", 4),
  ("Bahorel", "Feuilly", 3),
  ("Bahorel", "Prouvaire", 2),
  ("Bahorel", "Marius", 1),
  ("Bossuet", "Marius", 5),
  ("Bossuet", "Courfeyrac", 12),
  ("Bossuet", "Gavroche", 5),
  ("Bossuet", "Bahorel", 4),
  ("Bossuet", "Enjolras", 10),
  ("Bossuet", "Feuilly", 6),
  ("Bossuet", "Prouvaire", 2),
  ("Bossuet", "Combeferre", 9),
  ("Bossuet", "Mabeuf", 1),
  ("Bossuet", "Valjean", 1),
  ("Joly", "Bahorel", 5),
  ("Joly", "Bossuet", 7),
  ("Joly", "Gavroche", 3),
  ("Joly", "Courfeyrac", 5),
  ("Joly", "Enjolras", 5),
  ("Joly", "Feuilly", 5),
  ("Joly", "Prouvaire", 2),
  ("Joly", "Combeferre", 5),
  ("Joly", "Mabeuf", 1),
  ("Joly", "Marius", 2),
  ("Grantaire", "Bossuet", 3),
  ("Grantaire", "Enjolras", 3),
  ("Grantaire", "Combeferre", 1),
  ("Grantaire", "Courfeyrac", 2),
  ("Grantaire", "Joly", 2),
  ("Grantaire", "Gavroche", 1),
  ("Grantaire", "Bahorel", 1),
  ("Grantaire", "Feuilly", 1),
  ("Grantaire", "Prouvaire", 1),
  ("MotherPlutarch", "Mabeuf", 3),
  ("Gueulemer", "Thenardier", 5),
  ("Gueulemer", "Valjean", 1),
  ("Gueulemer", "Mme.Thenardier", 1),
  ("Gueulemer", "Javert", 1),
  ("Gueulemer", "Gavroche", 1),
  ("Gueulemer", "Eponine", 1),
  ("Babet", "Thenardier", 6),
  ("Babet", "Gueulemer", 6),
  ("Babet", "Valjean", 1),
  ("Babet", "Mme.Thenardier", 1),
  ("Babet", "Javert", 2),
  ("Babet", "Gavroche", 1),
  ("Babet", "Eponine", 1),
  ("Claquesous", "Thenardier", 4),
  ("Claquesous", "Babet", 4),
  ("Claquesous", "Gueulemer", 4),
  ("Claquesous", "Valjean", 1),
  ("Claquesous", "Mme.Thenardier", 1),
  ("Claquesous", "Javert", 1),
  ("Claquesous", "Eponine", 1),
  ("Claquesous", "Enjolras", 1),
  ("Montparnasse", "Javert", 1),
  ("Montparnasse", "Babet", 2),
  ("Montparnasse", "Gueulemer", 2),
  ("Montparnasse", "Claquesous", 2),
  ("Montparnasse", "Valjean", 1),
  ("Montparnasse", "Gavroche", 1),
  ("Montparnasse", "Eponine", 1),
  ("Montparnasse", "Thenardier", 1),
  ("Toussaint", "Cosette", 2),
  ("Toussaint", "Javert", 1),
  ("Toussaint", "Valjean", 1),
  ("Child1", "Gavroche", 2),
  ("Child2", "Gavroche", 2),
  ("Child2", "Child1", 3),
  ("Brujon", "Babet", 3),
  ("Brujon", "Gueulemer", 3),
  ("Brujon", "Thenardier", 3),
  ("Brujon", "Gavroche", 1),
  ("Brujon", "Eponine", 1),
  ("Brujon", "Claquesous", 1),
  ("Brujon", "Montparnasse", 1),
  ("Mme.Hucheloup", "Bossuet", 1),
  ("Mme.Hucheloup", "Joly", 1),
  ("Mme.Hucheloup", "Grantaire", 1),
  ("Mme.Hucheloup", "Bahorel", 1),
  ("Mme.Hucheloup", "Courfeyrac", 1),
  ("Mme.Hucheloup", "Gavroche", 1),
  ("Mme.Hucheloup", "Enjolras", 1)
]

def rst(s, *repls): 
    '''Really stupid templates
       Yeah, so templates might be better. Meh.'''
    for name, value in repls:
        s = s.replace('${'+name+'}', str(value))
    return s

def render_d3_fdd(dat, scale=1, force_scale=1, canvas_wh=(800, 800), save_freq='null', click_function=click_default):
    f = '/tmp/index.html'
    w, h = canvas_wh
    s = rst(templ, ('scale', scale),
                   ('force_scale', force_scale),
                   ('canvasw', w),
                   ('canvash', h),
                   ('save_freq', save_freq),
                   ('click_function', click_function),
                   ('graph', json.dumps(dat)),
    )
    with open(f, 'w') as fid:
        fid.write(s)
    
    os.system('xdg-open '+f)

def fdd(nodes, links, **kwds):
    d = OrderedDict([
        ("nodes", [OrderedDict([("id", _id), ("group", group)])
                  for _id, group in nodes]),
        ("links", [OrderedDict([("source", source), ("target", target), ("value", value)])
                  for source, target, value in links]),
    ])
    return render_d3_fdd(d, **kwds)

def do_cmd(cmd):
    print cmd
    return os.system(cmd)

def file_stem(filename):
    return os.path.splitext(os.path.split(filename)[-1])[0]

def string_between(s, before, after):
    return s.split(before)[1].split(after)[0]

def _generate_pngs(svg_base, dout, out_base, png_wh):
    w, h = png_wh
    for fin in glob.glob(svg_base):
        f = file_stem(fin)
        f = 'newesttree (0)' if f == 'newesttree' else f # For consistency
        i = int(string_between(f, '(', ')'))
        fout = os.path.join(dout, '{}_{:03d}.png'.format(out_base, i))
        do_cmd('inkscape -z -e "{fout}" -w {w} -h {h} "{fin}" -y=1'
               .format(fin=fin, fout=fout, w=w, h=h))

def _generate_gif(dout, out_base, animation_delay=20, display=True):
    pngs = os.path.join(dout, out_base+ '_*.png')
    gif = os.path.join(dout, 'animation.gif')
    do_cmd('convert -delay {delay} -loop 0 {pngs} {gif}'.format(delay=animation_delay,
                                                                pngs=pngs, gif=gif))
    if display:
        do_cmd('eog {}'.format(gif))

def _handle_save_freq_options(save_freq, total_steps=300):
    '''Handle multiple string options for save_freq (see docs for fdd_plus_images)
       total_steps is the number of time steps d3 seems to takes in the sim (always 300?)
       Returns:
       save_freq: JS-friendly format (integer or 'null')
       ignore_first: boolean flag that controls which svgs get processed to images'''
    ignore_first = (save_freq == 'last')
    sf_dict = {None: 'null',
               'first_last': total_steps-1,
               'last': total_steps-1,
               'first': 10000000,
               'all': 1,
               -1: total_steps-1, # ??
              }
    save_freq = sf_dict[save_freq] if save_freq in sf_dict else save_freq
    return save_freq, ignore_first

def fdd_plus_images(nodes, links,
                    save_freq='last',
                    png_wh=(1200, 1200),
                    sleep_time=10,
                    out_base='out',
                    dout='/tmp/',
                    clean_downloads=True,
                    clean_tmp=True,
                    animate=True,
                    animation_delay=20,
                    display=True,
                    **kwds
                   ):
    '''Render a D3 graph, save svg's at various points, and then use
       inkscape and ImageMagick (convert) to create pngs and then an
       animated gif
       Input kwd args:
       save_freq: Control the number of svg's saved from the simulation
                  one of: None or 'null', 'last' (default), 'first', 'first_last', 'all'
                          or any integer
       png_wh: Canvas size of output pngs, default (1200, 1200)
       sleep_time: Time to wait before starting the png conversion, default 10s
       out_base: name of the output png files default 'out'
       dout: output directory, default '/tmp/'
       clean_downloads: When True (default), clear the Downloads folder of names like newesttree*.svg
       clean_tmp: When True (default), clear the output directory of names matching the output pattern
       animate: When True (default), create an animated gif from the generated pngs
       display: When True (default), open the gif with eog
       
       All other **kwds get passed to render_d3_fdd thru fdd'''
    svg_base = os.path.expanduser('~/Downloads/newesttree*.svg')
    save_freq, ignore_first = _handle_save_freq_options(save_freq)
    if clean_downloads:
        do_cmd('rm '+ svg_base)
    if clean_tmp:
        do_cmd('rm {}_*.png'.format(os.path.join(dout, out_base)))
    if ignore_first:
        svg_base = svg_base.replace('*', ' (*)')
    fdd(nodes, links, save_freq=save_freq, **kwds)
    
    if save_freq != 'null':
        time.sleep(sleep_time)
        _generate_pngs(svg_base, dout, out_base, png_wh)
        
        if animate:
            _generate_gif(dout, out_base, animation_delay=animation_delay, display=display)

if __name__ == '__main__':
    #fdd(sample_nodes, sample_links, save_freq=60) # version that auto-DL's svgs
    #fdd_plus_images(sample_nodes, sample_links, save_freq=20) # version that auto-DL's svg's AND converts to pngs and animated gif
    #fdd_plus_images(sample_nodes, sample_links) # saves just the image of the final render
    fdd_plus_images(sample_nodes, sample_links, save_freq=None) # disabled saving, best for testing
    
