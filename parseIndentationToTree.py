'''Turn a heirarchically indented text file into a nested
   list of strings (lines)'''

def getindentandstring(line):
    stripLine=line.lstrip()
    indent=line[:(len(line)-len(stripLine))]
    return indent,stripLine.replace('\r','').replace('\n','')

def indentationToListTree(fid,ignored=('')):
    '''Loop-based indentation to list tree maker
       lineage is a tree/branch data structure with the following features:
        * lineage[0] is the tree being built (trunk),
        * lineage is the same length as the depth of the tree
        * lineage[i] is a branch of the tree at level i leading to the current node
        * lineage[-1] is the current list of nodes,
        tablineage is always the same length as lineage and contains the leading
        whitespace at each depth of the tree'''
    
    lineage,tablineage=[[]],['']
    for line in fid:
        t,r=getindentandstring(line)
        if r in ignored:                  # ignore blank lines
            continue
        #print (t,r)
        if len(t)>len(tablineage[-1]):    # there is more indentation than before
                newl=[r]                  # make a new branch
                lineage[-1].append(newl)  # append it to the tree
                lineage.append(newl)      # append it to the lineage
                tablineage.append(t)      # append the whitespace to the tablineage
        else:
            for tab in tablineage[::-1]:        # check back up the tree to match whitespace
                if t==tab:                # there is the same indentation as this level
                    lineage[-1].append(r) # add the node to this branch
                    break                 # and stop
                else:                     # there is less indentation than before
                    lineage.pop()         # move to the parent branch of the current branch in the lineage
                    tablineage.pop()      # ... and also in the tab lineage
            assert len(lineage)>0 and len(tablineage)>0, 'Malformatted indent file: indentation not aligned properly!!'
    return lineage[0]                     # return the final tree

if __name__=='__main__':
    s='''a
b
c
    1
    2
        A
        B
    3
    4
        A
    5
d
e'''
    fakefid = ( i for i in s.split('\n'))
    print indentationToListTree(fakefid)
