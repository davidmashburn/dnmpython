from scipy.optimize import minimize,fmin
from np_utils import docAppend

def maximize(fun, x0, **kwds):
    '''maximize is a wrapper around scipy.optimize.minimize
    The only difference is that the fun argument is automatically adjusted to
    return the negative of the original.
    
    Documentation for minimize below:'''
    def negfun(*a,**k):
        '''Negative of the original fun argument to maximize...
           Original documentation below:'''
        return -fun(*a,**k)
    docAppend(negfun,fun)
    return minimize(negfun,x0,**kwds)
docAppend(maximize,minimize)

def fmax(func, x0,**kwds):
    '''fmax is a wrapper around scipy.optimize.fmin
    The only difference is that the fun argument is automatically adjusted to
    return the negative of the original.
    
    Documentation for fmin below:'''
    def negfunc(*a,**k):
        '''Negative of the original fun argument to maximize...
           Original documentation below:'''
        return -func(*a,**k)
    docAppend(negfunc,func)
    return fmin(negfunc,x0,**kwds)
docAppend(fmax,fmin)
