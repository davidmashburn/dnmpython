#!/usr/bin/env python

"""This one-liner script creates a worthless wxPython window to delay execution.
When this script is imported, it prevents the parent script from terminating
  until this window is closed.
Alternatively, a script could be run from the shell to acheive the same end.
Another alternative is to call
  app = wx.App(0)
  app.MainLoop()
but this will only work if you are using a wx program like pylab."""

__author__ = "David N. Mashburn <david.n.mashburn@gmail.com>"

import wx;app=wx.App();wx.MessageBox('Close to continue/finish')
