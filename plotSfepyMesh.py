import itertools
import numpy
import numpy as np
from mayavi import mlab
from sfepy import data_dir
from sfepy.fem import Mesh

def plotPoints(coors,scale_factor=0.001,color=(1,0,0)):
    mlab.points3d(coors.T[0],coors.T[1],coors.T[2],[1]*len(coors),scale_mode='none',scale_factor=scale_factor,color=color)
def plotLines(m):
    # Fast lines...
    x,y,z,s = m.coors.T[0],m.coors.T[1],m.coors.T[2],[1]*len(m.coors)
    connections=[]
    for c in m.conns[0]:
        connections+=list(itertools.combinations(c,2))
    connections=np.array(connections)

    # Create the points
    src = mlab.pipeline.scalar_scatter(x, y, z, s)
    # Connect them
    src.mlab_source.dataset.lines = connections
    # The stripper filter cleans up connected lines
    lines = mlab.pipeline.stripper(src)
    # Finally, display the set of lines
    mlab.pipeline.surface(src, colormap='Accent', line_width=1, opacity=.4)
def plotPointsAndLines(m):
    plotPoints(m.coors)
    plotLines(m)
oneRun=[1]
def plotTetra(vertices,scale_factor=1):
    if 0<=scale_factor<=1:
        x,y,z = np.transpose(vertices)
        if oneRun[0]:
            oneRun[0]=0
            print x,y,z,np.mean(x),np.mean(y),np.mean(z)
        x=(x*scale_factor+np.mean(x)*(1-scale_factor))
        y=(y*scale_factor+np.mean(y)*(1-scale_factor))
        z=(z*scale_factor+np.mean(z)*(1-scale_factor))
        
        triangles = list(itertools.combinations([0,1,2,3],3))
        mlab.triangular_mesh(x, y, z, triangles)
    else:
        print 'scale_factor must be between 0 and 1; it weights the coordinates towards the mean'

def plotAllTetras(m,scale_factor=1,condition=None):
    '''m is a Mesh object
    condition is a function that acts on a point like: lambda x: (x[0]<0.012)'''
    for i in range(len(m.conns[0])):
        pts=[m.coors[j] for j in m.conns[0][i]]
        if condition==None:
            plotTetra(pts,scale_factor=scale_factor)
        elif sum([condition(p) for p in pts])==4:
            plotTetra(pts,scale_factor=scale_factor)

if __name__=='__main__':
    m = Mesh.from_file(data_dir+"/meshes/3d/cylinder.vtk")
    mlab.clf()
    plotPointsAndLines(m)
    blue = m.coors[np.where(m.coors[:,0]<0.01)]
    plotPoints(blue,color=(0,0,1))
    plotAllTetras( m, scale_factor=0.3, condition=(lambda x: (x[0]<0.012)) )
    #for i in range(len(m.conns[0])):
    #    pts=[m.coors[j] for j in m.conns[0][i]]
    #    if sum([p[0]<0.012 for p in pts])==4:
    #        plotTetra(pts,scale_factor=0.3)
    mlab.show() #start blocking to keep window open
