import os
import glob
import time

#l=os.listdir('.')
#l=glob.glob('*.py')

def TestString(fileName,old,repl=''):
    fid=open(fileName)
    
    for s in fid:
        if(s.count(old)>0):
            print '    ',s
    
    fid.close()

def TestStrings(fileName,old,repl=''):
    fid=open(fileName)
    
    for s in fid:
        for j in old:
            if(s.count(j)>0):
                print '    ','    ',s
    
    fid.close()

def Replace(fileName,old,repl):
    fid=open(fileName)
    
    S=[]
    
    for s in fid:
        S.append(s.replace(old,repl))
    
    fid.close()
    
    fid=open(fileName,'w')
    
    for i in S:
        fid.write(i)
    
    fid.close()

def CompareMd5Contents(fileName1=None,fileName2=None): #Warning -- UnTested!!!
    import wx
    import hashlib
    if fileName1==None:
        fileName1=wx.FileSelector()
    if fileName2==None:
        fileName2=wx.FileSelector()
    f1=open(fileName1)
    f2=open(fileName2)
    r1=f1.read()
    r2=f2.read()
    f1.close()
    f2.close()
    
    md1=hashlib.md5()
    md2=hashlib.md5()
    md1.update(r1)
    md2.update(r2)
    bin1=md1.digest()
    bin2=md2.digest()
    
    return bin1==bin2

#More work to be done here to make this better!
# Add extensions to list of arguments

def DirectoryFind(d=None,test=['MyColormaps'],extList=['.py','.pyx','.pxd']):
    if d==None:
        d=os.getcwd()
    print d

    for k in os.listdir('.'):
        ext=os.path.splitext(k)[1]
        
        if os.path.isdir(os.path.join(d,k)):
            print k
            os.chdir(os.path.join(d,k))
            # Remeber that + means concatenate lists, not add here
            l=[]
            for i in extList:
                l+=glob.glob('*'+i)
            for i in l:
                print '    ', i
                TestStrings(i,test)
            os.chdir(d)
        elif extList.count(ext): # if ext is one of the extensions given in extList
            print k
            TestStrings(k,test)
            os.chdir(d)

def DirectoryReplace(d=None,old='n.Float',new='n.float'): #use with caution!
    if d==None:
        d=os.getcwd()
    print d

    for k in os.listdir('.'):
        ext=os.path.splitext(k)[1]
        
        if(ext==''):
            print k
            os.chdir(os.path.join(d,k))
            # Remeber that + means concatenate lists, not add here
            l=glob.glob('*.py')+glob.glob('*.pyx')+glob.glob('*.pxd')
            for i in l:
                print '    ', i
                Replace(i,old,new)
            os.chdir(d)
        elif(ext=='.py' or ext=='.pyx' or ext=='.pxd'):
            print k
            Replace(k,old,new)
            os.chdir(d)

def DirectoryCount(dir,countDirs=False,countFiles=True):
    count=0
    for i in glob.glob(os.path.join(dir,'*')):
        if os.path.isdir(i):
            count+=DirectoryCount(i,countDirs=countDirs,countFiles=countFiles)
            if countDirs:
                count+=1
        else:
            if countFiles:
                count+=1
    return count

def SuperSimpleDirectoryWrite(d='/home/mashbudn/Pictures',printAll=False,saveFile=False):
    os.chdir(d)
    d=os.getcwd()
    w=os.walk(d)
    bigList=[]
    while True:
        try:
            [d,sub,files]=w.next()
            print d
        except StopIteration:
            break
        
        sd=os.path.split(d)
        d_n_files=[os.path.join(d,i) for i in files]
        fst=[[files[i],os.path.getsize(d_n_files[i]),os.path.getmtime(d_n_files[i])] for i in range(len(files))]
        bigList.append([sd[-1],os.path.join(sd[:-1])[0],len(sub),fst])
    
    if printAll:
        print '['
        for i in bigList:
            print i,',\n'
        print ']'
    
    if saveFile:
        fid=open('/home/mashbudn/FileDirOut.py','w')
        fid.write('d=[\n')
        for i in bigList:
            fid.write(str(i))
            fid.write(',\n\n')
        fid.write(']\n')
    
    return bigList

def CompareDirectories(d1,d2,compareFileInternals=False,depth=0,extToIgnore=['.pyc']):
    w1=os.walk(d1)
    w2=os.walk(d2)
    [d1,sub1,files1]=w1.next()
    [d2,sub2,files2]=w2.next()
    
    ddiff1=list(set(sub2).difference(set(sub1))) # Directories of 2 not in 1
    ddiff2=list(set(sub1).difference(set(sub2))) # Directories of 1 not in 2
    dintersection=list(set(sub1).intersection(sub2)) #Directories in both
    if len(ddiff1)>0:
        print 'Directories not in',d1,':'
        for i in ddiff1:
            print i
        print ''
    if len(ddiff2)>0:
        print 'Directories not in',d2,':'
        for i in ddiff2:
            print i
        print ''
    
    fdiff1=list(set(files2).difference(set(files1))) # Directories of 2 not in 1
    fdiff2=list(set(files1).difference(set(files2))) # Directories of 1 not in 2
    fintersection=list(set(files1).intersection(files2)) #Files in both
    if len(fdiff1)>0:
        print 'Files not in',d1,':'
        for i in fdiff1:
            print i
        print ''
    if len(fdiff2)>0:
        print 'Files not in',d2,':'
        for i in fdiff2:
            print i
        print ''
    
    for i in fintersection:
        f1=os.path.join(d1,i)
        f2=os.path.join(d2,i)
        
        size1=os.path.getsize(f1)
        size2=os.path.getsize(f2)
        time1=os.path.getmtime(f1)
        time2=os.path.getmtime(f2)
        if size1 != size2 or abs(time1- time2)>3:
            if extToIgnore.count(os.path.splitext(i)[1])==0: # Not a skipped extension like .pyc
                print i
                print 'Size 1:',size1
                print 'Size 2:',size2
                print 'Time 1:',time.strftime("%Y/%m/%d %H:%M:%S",time.strptime(time.ctime(time1)))
                print 'Time 2:',time.strftime("%Y/%m/%d %H:%M:%S",time.strptime(time.ctime(time2)))
                print ''
            if compareFileInternals:
                print 'Not Implemented'
                if 0:
                    if max(size1,size2)>=1048576:# 1 MB
                        print 'One of the '+i+'files is too big to compare'
                        continue
                    
                    fid1=open(f1,'r')
                    fid2=open(f2,'r')
                    lines1=fid1.readlines()
                    lines2=fid2.readlines()
                    for i in range(min(len(lines1),len(lines2))):
                        pass
                    # Print the rest of the larger file
                    print (lines1,lines2)[len(lines1)<=len(lines2)][:max(len(lines1),len(lines2))]
    
    if depth>0:
        for i in dintersection:
            CompareDirectories(os.path.join(d1,i),os.path.join(d2,i))


##OLD
##f1=glob.glob(d1+'\\*')
##f2=glob.glob(d2+'\\*')
##
##for i in f1:
##    match=0
##    for j in f2:
##        if os.path.split(i)[1]==os.path.split(j)[1]:
##            match=1
##            break
##    if not match:
##        print os.path.split(i)[1], ' not in ',d2
##print '\n'
##for i in f2:
##    match=0
##    for j in f1:
##        if os.path.split(i)[1]==os.path.split(j)[1]:
##            match=1
##            break
##    if not match:
##        print os.path.split(i)[1], ' not in ',d1
##
##def Print1WayCompare(big,smallName,type='Files'): # check to see what elements from 'big' are missing in 'small' (big & small may not reflect actual sizes...)
##    print type,'not in',smallName,':'
##    for i in big:
##        if not small.count(i):
##            print '    ',i
##    print '\n'

# On the terminal to move the "01" stuff to the Copies folder
#mkdir Copies
#mv *\ 1* Copies

# To remove the " 1" 's
#import glob
#for i in glob.glob('*'):
#    os.rename(i,i.replace(' 1',''))

def FindDuplicatesByNameAndSize():
    a=[]
    for i in glob.glob('*'):
        a.append([os.stat(i)[7],i])
    a.sort()
    b=[i[0] for i in a]
    ind=where(diff(b)==0)[0]
    for i in ind:
        if b[i]==b[i+1]:
            if a[i][1][:8]==a[i+1][1][:8]:
                print a[i][1],i


if __name__=='__main__':
    DirectoryFind()
