def centerPt(i):
    return 'Point('+str(i)+') = {0, 0, 0, cl1};'

def BasicSquare(i,z=0):
    return '''Point('''+str(i)+''') = {-1, -1, '''+str(z)+''', cl1};
Point('''+str(i+1)+''') = {1, -1, '''+str(z)+''', cl1};
Point('''+str(i+2)+''') = {-1, 1, '''+str(z)+''', cl1};
Point('''+str(i+3)+''') = {1, 1, '''+str(z)+''', cl1};'''

def PrtPnt(i,coords,clStr):
    return 'Point('+str(i)+') = {'+repr(coords)[1:-1]+', '+clStr+'};'

def PrtCon(conStr,i,*args):
    return conStr+'('+str(i)+') = {'+repr(list(args))[1:-1]+'};'

def centerPt(i):
    return PrtPnt(i,[0, 0, 0], 'cl1')

def BasicShape(xys,pi,li,clStr,z=0):
    if hasattr(clStr,'__len__'): # allow passing a list or a value for clStr
        if len(clStr)!=len(xys):
            raise IndexError, 'xys and clStr must have the same length!'
            return
    else:
        clStr = [clStr]*len(xls)
    
    return { 'points': '\n'.join([PrtPnt(pi+i,xys[i]+[z], clStr[i]) for i in range(len(xys))]),
             'lines' : '\n'.join([PrtCon('Line',li+i,pi+i,pi+(i+1)%len(xys)) for i in range(len(xys))])}
def BasicSquare(pi,li,clStr,z=0):
    xys=[[-1,-1],[1,-1],[1,1],[-1,1]]
    return BasicShape(xys,pi,li,clStr,z=z)

def ThickSlice(xys,pi,li,bottom,top,clStr):
    dbot = BasicShape(xys,pi,li,clStr,z=top)
    dtop = BasicShape(xys,pi+len(xys),li+len(xys),clStr,z=bottom)
    connLines = '\n'.join([ PrtCon('Line', li+2*len(xys)+i, pi+i, pi+i+len(xys)) for i in range(len(xys)) ])
    return { 'points': '\n'.join([dbot['points'],dtop['points']]),
             'lines' : '\n'.join([dbot['lines'],dtop['lines'],connLines])}

def ThickSandwich(xys,pi,li,layers,clStr): ### DONE -- TEST ME ###
    if not hasattr(layers,'__len__'):
        raise TypeError, 'layer must be a list'
        return
    if len(layers)<2:
        raise IndexError, 'layers must have at least two elements!'
        return
    dList=[]
    for i in range(len(layers)):
        dList.append( BasicShape(xys,pi+i*len(xys),li+i*len(xys),clStr,z=layers[i]) )
    nli = li+len(layers)*len(xys) # new li (list index)
    connLines = []
    for i in range(len(layers)-1):
        for j in range(len(xys)):
            connLines.append( PrtCon('Line', nli+i*len(xys)+j, pi+i*len(xys)+j, pi+(i+1)*len(xys)+j) )

    return { 'points': '\n'.join([d['points'] for d in dList]),
             'lines' : '\n'.join([d['lines'] for d in dList]+connLines)}

def ThickSliceBoundarySurfaces(li,si,liStart,numPts):
    '''li is the current line index that lineloop and planesurface have to use, liStart was the line index of the first line of interest in the thick slice'''
    #1st third of lines are the bottom, 2nd third are top, final third are connections
    r = range(numPts)
    bottom = [liStart+i          for i in r]
    top    = [liStart+i+numPts   for i in r]
    conn   = [liStart+i+2*numPts for i in r]
    # formula for edge surface is: bottom, conn2,-top,-conn1
    l=[]
    for i in range(numPts):
        l.append(PrtCon('Line Loop',li+i, bottom[i],conn[(i+1)%numPts],-top[i],-conn[i]))
        l.append(PrtCon('Plane Surface',si+i, li+i))
    return '\n'.join(l)

def ThickSandwichBoundarySurfaces(li,si,liStart,numPts,numLayers): ### DONE -- TEST ME ###
    '''li is the current line index that lineloop and planesurface have to use, liStart was the line index of the first line of interest in the thick slice'''
    #liStart is the beginning of the perimeter loops
    nliStart = liStart + numPts*numLayers # Beginning of the connectors
    
    perimeterLineList = [[liStart+i+j*numPts  for i in range(numPts)] for j in range(numLayers)]   # aka bottom through top
    connLineList      = [[nliStart+i+j*numPts for i in range(numPts)] for j in range(numLayers-1)] # all stacks of connectors; one less than the number of layers
    
    # formula for edge surface is: bottom, conn2,-top,-conn1
    l=[]
    for j in range(numLayers-1):
        for i in range(numPts):                        #bottom                  #conn2                         #-top                       #-conn1
            l.append(PrtCon('Line Loop',li+i+j*numPts, perimeterLineList[j][i], connLineList[j][(i+1)%numPts], -perimeterLineList[j+1][i], -connLineList[j][i]))
            l.append(PrtCon('Plane Surface',si+i+j*numPts, li+i+j*numPts))
    return '\n'.join(l)

def ThickBoundaryLoops(li,liStart,numPts,numLayers=2):
    return '\n'.join([PrtCon('Line Loop',li+i, *range(liStart+i*numPts,liStart+(i+1)*numPts)) for i in range(numLayers)])

def ThickSquare(pi,li,top,bottom,clStr):
    xys=[[-1,-1],[-1,1],[1,1],[1,-1]]
    return ThickSlice(xys,pi,li,top,bottom,clStr)

def IncrInd(pi,li,lastDict):
    pi += len(lastDict['points'].split('\n'))
    li += len(lastDict['lines'].split('\n'))
    return pi,li

def GenerateThickSliceWithHoles(outerBoundary,innerBoundaries,bottom,top): # might add meshing cl option by loop later
    '''Create a prismatic solid .geo file with outer and inner boundaries'''
    resultStr = ['cl1 = 0.1;']
    clStr='cl1'
    pi,li,si,vi=0,0,0,0
    liHistory=[0]
    resultStr.append( centerPt(pi) )
    pi+=1
    
    tsO = ThickSlice(outerBoundary,pi,li,bottom,top,'cl1')
    pi,li = IncrInd(pi,li,tsO)
    liHistory.append(li)
    
    tsis = []
    for b in innerBoundaries:
        tsis.append( ThickSlice( b ,pi,li,bottom,top,'cl1') )
        pi,li = IncrInd(pi,li,tsis[-1])
        liHistory.append(li)
    
    resultStr.append( tsO['points'] )
    for tsi in tsis:
        resultStr.append( tsi['points'] )
    resultStr.append( tsO['lines'] )
    for tsi in tsis:
        resultStr.append( tsi['lines'] )
    
    resultStr.append( ThickSliceBoundarySurfaces(li,si,liHistory[0],len(outerBoundary)) )
    li+=len(outerBoundary)
    si+=len(outerBoundary)
    #liHistory.append(li)
    
    for i,b in enumerate(innerBoundaries):
        resultStr.append( ThickSliceBoundarySurfaces(li,si,liHistory[i+1],len(b)) )
        li+=len(b)
        si+=len(b)
    resultStr.append( ThickBoundaryLoops(li,0,len(outerBoundary)) )
    li+=2
    for i,b in enumerate(innerBoundaries):
        resultStr.append( ThickBoundaryLoops(li,liHistory[i+1],len(b)) )
        li+=2
    resultStr.append( PrtCon('Plane Surface',si,li-4,-(li-2)) )
    si+=1
    resultStr.append( PrtCon('Plane Surface',si,li-3,-(li-1)) )
    si+=1
    resultStr.append( PrtCon('Surface Loop',0, *range(si)) )
    resultStr.append( PrtCon('Volume',0, 0) )
    resultStr.append( PrtCon('Physical Point',0,*range(2,pi)) )
    resultStr.append( PrtCon('Physical Line',0,*range(liHistory[-1])) )
    resultStr.append( PrtCon('Physical Surface',0,*range(liHistory[-1],li)) )
    resultStr.append( PrtCon('Physical Volume',0, 0) )
    
    return '\n'.join(resultStr)

def GenerateThickSandwichWithHoles(outerBoundary,innerBoundaries,layers,clsStr='cl1 = 0.1;',clsOuterList=None,clsInnerList=None): # might add meshing cl option by loop later
    '''Create a muli-layer prismatic solid .geo file with outer and inner boundaries''' ### DONE -- TEST ME ###
    
    if not hasattr(layers,'__len__'):
        raise TypeError, 'layer must be a list'
        return
    if len(layers)<2:
        raise IndexError, 'layers must have at least two elements!'
        return
    
    if clsOuterList==None:
        clsOuterList = ['cl1']*len(outerBoundary)
    if clsInnerList==None:
        clsInnerList = [['cl1']*len(i) for i in innerBoundaries]
    
    # Get a list of all clsVals that were passed as arguments
    clsVals = sorted(set(clsOuterList+[i for j in clsInnerList for i in j])) # outer list + flattened inner list
    for i in clsVals:
        if i not in clsStr:
            print 'Warning!',i,'is not defined in the cls string!'
    
    resultStr = [clsStr]
    pi,li,si,vi=0,0,0,0
    liHistory=[0]
    resultStr.append( centerPt(pi) )
    pi+=1
    
    tsO = ThickSandwich(outerBoundary,pi,li,layers,clsOuterList)
    pi,li = IncrInd(pi,li,tsO)
    liHistory.append(li)
    
    tsis = []
    for i,b in enumerate(innerBoundaries):
        tsis.append( ThickSandwich( b ,pi,li,layers,clsInnerList[i]) )
        pi,li = IncrInd(pi,li,tsis[-1])
        liHistory.append(li)
    
    resultStr.append( tsO['points'] )
    for tsi in tsis:
        resultStr.append( tsi['points'] )
    resultStr.append( tsO['lines'] )
    for tsi in tsis:
        resultStr.append( tsi['lines'] )
    
    edgeSurfaceIndices=[None]*(len(layers)-1) # all the indices of edge surfaces in each layer
    resultStr.append( ThickSandwichBoundarySurfaces(li,si,liHistory[0],len(outerBoundary),len(layers)) )
    for j in range(len(layers)-1):
        edgeSurfaceIndices[j]=range(si,si+len(outerBoundary))
        li+=len(outerBoundary)
        si+=len(outerBoundary)
        #liHistory.append(li)
    
    for i,b in enumerate(innerBoundaries):
        resultStr.append( ThickSandwichBoundarySurfaces(li,si,liHistory[i+1],len(b),len(layers)) )
        for j in range(len(layers)-1):
            edgeSurfaceIndices[j]+=range(si,si+len(b))
            li+=len(b)
            si+=len(b)
            #liHistory.append(li)
    
    loopIndices=[] # so begins the loop index for the bottom of each "tower"...
    resultStr.append( ThickBoundaryLoops(li,0,len(outerBoundary),len(layers)) )
    loopIndices.append(li)
    li+=len(layers)
    for i,b in enumerate(innerBoundaries):
        resultStr.append( ThickBoundaryLoops(li,liHistory[i+1],len(b),len(layers)) )
        loopIndices.append(li)
        li+=len(layers)
    
    bottomSurfaceIndex = si
    for i in range(len(layers)):
        resultStr.append( PrtCon('Plane Surface',si,loopIndices[0]+i,*[-(loopIndices[j+1]+i) for j in range(len(innerBoundaries))]) )
        si+=1
    
    for i in range(len(layers)-1):
        surfaces = edgeSurfaceIndices[i] + [bottomSurfaceIndex+i,bottomSurfaceIndex+i+1]
        resultStr.append( PrtCon('Surface Loop',i,*surfaces) )
        resultStr.append( PrtCon('Volume',i, i) )
    resultStr.append( PrtCon('Physical Point',0,*range(2,pi)) )
    resultStr.append( PrtCon('Physical Line',0,*range(liHistory[-1])) )
    resultStr.append( PrtCon('Physical Surface',0,*range(liHistory[-1],li)) )
    resultStr.append( PrtCon('Physical Volume',0, *range(len(layers)-1)) )
    
    return '\n'.join(resultStr)
    # Old notes describing what this does:
    # Need to make a sandwich version of this so I can have 3 layers:
    #make more points -- from 2 to n
    #make more loop lines -- from 2 to n
    #make more connectors sets -- from 1 to n-1
    #make more bulk surfaces -- from 2 to n
    #make more edge surfaces -- from 1 to n-1

if __name__=='__main__':
    #a test
    layers=[0,0.06,0.09,0.15]
    #layers=[0,0.09,0.11,0.2]
    s = GenerateThickSandwichWithHoles([[-1,-1],[1,-1],[1,1],[-1,1]],[ [[0, -0.1],[0.1, 0],[0, 0.1],[-0.1, 0]] ],layers=layers,clsStr='cl1 = 0.1;\ncl2 = 0.02;',clsOuterList=['cl1']*4,clsInnerList=[['cl2']*4]) # might add meshing cl option by loop later
    print s
    #fid = open('/home/mashbudn/Documents/VIIBRE--ScarHealing/FiniteElementModeling/sfepy/DiamondCakeHoleTractionIsotropic3D/test2lines_B.geo','w')
    #fid.write(s)
    #fid.close()
    
    #another test
    print GenerateThickSliceWithHoles([[-1,-1],[1,-1],[1,1],[-1,1]],[ [[0, -0.1],[0.1, 0],[0, 0.1],[-0.1, 0]] ],-0.1,0.1) # might add meshing cl option by loop later
    #[[0, -0.1],[0.1, 0],[0, 0.1],[-0.1, 0]]
    
    #another test
    print centerPt(1)
    ts1 = ThickSlice([[-1,-1],[1,-1],[1,1],[-1,1],[0.1,0.1]],2,0,0.1,-0.1,'cl1')
    print ts1['points']
    print ts1['lines']
    
    #another test
    print PrtPnt(0,[-0.3,0.7,1],'cl1')
    print centerPt(1)
    bs2=BasicSquare(2,0,'cl2')
    bs2b=BasicSquare(6,4,'cl2',z=0.2)
    print bs2['points']
    print bs2b['points']
    print bs2['lines']
    print bs2b['lines']
    print PrtCon('Line',0,3,7)

