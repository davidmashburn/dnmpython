# Author: David Mashburn
# Written: 3/17/2008
# Modified: 6/12/2008

import numpy as n
import os
import vtk
from vtk.util.colors import tomato

from scipy.ndimage import gaussian_filter
gauss=gaussian_filter

import wx
from wxVTKRenderWindowInteractor import wxVTKRenderWindowInteractor
wxVTKRenderWindow=wxVTKRenderWindowInteractor

# This contains a "virtual" class that has to be subclassed

# It uses figures and plotting, 2D and 3D
# Everything is set upon plotting:
# Surf2D:    dat,cmap=None,cmin=-10,cmax=10,scale=[1,1],center=True
# Surf3D:    dat,cmap=None,cmin=-10,cmax=10,scale=[1,1,10],center=True,resetCamera=True
# Volume:    dat,opacitymap=[0,1],cmap=None,cmin=0,cmax=255,scale=[1,1,10],center=True,resetCamera=True
# This also uses pylab colormaps (in pylab.cm) which are pt,r,g,b
# This is a simple conversion to a color transfer function
# What the user passes is a pylab-style cmap, given as a dictionary:
# Here is a simple grayscale as an example:
# {'blue': ((0.0, 0, 0), (1.0, 1, 1)), 'green': ((0.0, 0, 0), (1.0, 1, 1)), 'red': ((0.0, 0, 0), (1.0, 1, 1))}

# This is a function to get values for Piecewise functions based on this format
# If val equals one of the x values in segment, returns the rightmost value, consistent with matplotlib
def PW(val,segments):
    s=list(map(list,segments))
    s.sort()
    if val<s[0][0]:
        return s[0][1]
    elif val>=s[-1][0]:
        return s[-1][2]
    else:
        for i in range(1,len(s)):
            if val<s[i][0]:
                return (s[i-1][2]*(s[i][0]-val)+s[i][1]*(val-s[i-1][0]))*1./(s[i][0]-s[i-1][0])

# This is function to decimate an array by reslicing it, aka remove 1/dec of the data along each axis
# (Or only keep points along all axes where x%dec==0)
# Could also easily do list-based for various scales...
def Decimate(arr,dec=2):
    d=arr # make a slice-copy...
    for i in range(arr.ndim):
        d=d.take(range(0,arr.shape[i],dec), axis=i)
    
    return d.copy() # Make the array own its own memory now...

# You can also do this in a one-liner by writing:
# s=z2.shape ; dec=2 ; #z2[ 0:s[0]:dec , 0:s[1]:dec , 0:s[2]:dec , ... ]

# This function is legacy only and not nearly as useful...
def Periodic_Delete(arr,dec=2):
    d=arr # make a slice-copy...
    for i in range(z2.ndim):
        d=n.delete(d, n.s_[0:z2.shape[i]:dec], axis=i)
    
    return d.copy() # Make the array own its own memory now...

class VTKPlotObject(object):
    def __init__(self,cmap=None,cmin=-1,cmax=1,useExternalRenWin=False,renWin=None,useWx=False): # Always maintain your own lookup table...reset it after init...
        "This creates an VTKPlotObject which essentially wraps some VTK functions"
        "It has methods for ImShow,Surf, and Volume3D plotting with pylab-style colormaps"
        if useExternalRenWin==True:
            if renWin==None:
                self.SkipRender=True
            else:
                self.renWin=renWin
        else:
            self.SkipRender=False
        
        self.useWx=useWx
        self.state='none' # What type of plot is currently active
        
        [self.NX,self.NY,self.NZ]=[1,1,1]
        
        # if this came from pylab.cm.xx, then automatically pull out the dictionary
        # otherwise, let the user build their own dictionary
        self.ctf=vtk.vtkColorTransferFunction()
        self.lut=vtk.vtkLookupTable()
        self.cmap=None # when cmap=None is passed, this tells SetColormap to load the default cmap (jet)
        self.SetColormap(cmap,cmin,cmax)
        
        self.vArr=vtk.vtkFloatArray()
        self.pts=vtk.vtkStructuredPoints()
        self.pts.SetScalarTypeToUnsignedShort()
        
        self.imgdat = vtk.vtkImageData()
        self.res=vtk.vtkImageReslice()
        self.res.SetInput(self.imgdat)
        
        self.imap2c=vtk.vtkImageMapToColors()
        self.imap2c.SetInputConnection(self.res.GetOutputPort())
        self.imap2c.SetOutputFormatToRGB()
        
        self.ren = vtk.vtkRenderer()
        
        # specifically for scatter:
        self.color=tomato
        self.inputPoints = vtk.vtkPoints()
        
        self.inputData = vtk.vtkPolyData()
        self.inputData.SetPoints(self.inputPoints)
        
        self.balls = vtk.vtkSphereSource() # Use sphere as glyph source.
        self.balls.SetRadius(.01)
        self.balls.SetPhiResolution(10)
        self.balls.SetThetaResolution(10)
        
        self.glyphPoints = vtk.vtkGlyph3D()
        self.glyphPoints.SetInput(self.inputData)
        self.glyphPoints.SetSource(self.balls.GetOutput())
        
        self.glyphMapper = vtk.vtkPolyDataMapper()
        self.glyphMapper.SetInputConnection(self.glyphPoints.GetOutputPort())
        
        self.glyph = vtk.vtkActor()
        self.glyph.SetMapper(self.glyphMapper)
        self.glyph.GetProperty().SetDiffuseColor(self.color)
        self.glyph.GetProperty().SetSpecular(.3)
        self.glyph.GetProperty().SetSpecularPower(30)
        
        # specifically for imaging
        self.mapper2D=vtk.vtkImageMapper()
        self.mapper2D.SetInputConnection(self.imap2c.GetOutputPort())
        
        self.actor2D=vtk.vtkActor2D()
        self.actor2D.SetMapper(self.mapper2D)
        
        self.mapper2D.SetColorWindow(255.0)
        self.mapper2D.SetColorLevel(127.5)
            
        self.SetColormap(cmap,cmin,cmax)
        
        # specifically for surface:
        self.geometry = vtk.vtkImageDataGeometryFilter()
        self.geometry.SetInputConnection(self.res.GetOutputPort())
        
        self.warp = vtk.vtkWarpScalar()
        self.warp.SetInputConnection(self.geometry.GetOutputPort())
        self.warp.UseNormalOn()
        self.warp.SetNormal(0,0,1)
        
        self.merge = vtk.vtkMergeFilter()
        self.merge.SetGeometry(self.warp.GetOutput())
        self.merge.SetScalars(self.imap2c.GetOutput())
        
        self.mapper = vtk.vtkDataSetMapper()
        self.mapper.SetInputConnection(self.merge.GetOutputPort())
        self.mapper.SetScalarRange(0, 255)
        self.mapper.ImmediateModeRenderingOff()
        self.actor = vtk.vtkActor()
        self.actor.SetMapper(self.mapper)
        
        #Specifically for volume
        self.opacityTransferFunction = vtk.vtkPiecewiseFunction()
        self.SetVolumeOpacity()
        self.volumeProperty = vtk.vtkVolumeProperty()
        self.volumeProperty.SetColor(self.ctf)
        self.volumeProperty.SetScalarOpacity(self.opacityTransferFunction)
        self.volumeProperty.ShadeOn()
        self.volumeProperty.SetInterpolationTypeToLinear()
        
##        # The mapper / ray cast function know how to render the data
##        self.compositeFunction = vtk.vtkVolumeRayCastCompositeFunction()
##        self.volumeMapper = vtk.vtkVolumeRayCastMapper()
##        self.volumeMapper.SetVolumeRayCastFunction(self.compositeFunction)
        
        # The mapper knows how to order the data
        self.volumeMapper = vtk.vtkVolumeTextureMapper2D()
        self.volumeMapper.SetInput(self.pts)
        
        # The volume holds the mapper and the property and can be used to
        # position/orient the volume
        self.volume = vtk.vtkVolume()
        self.volume.SetMapper(self.volumeMapper)
        self.volume.SetProperty(self.volumeProperty)
        
        if not self.SkipRender:
            if useWx:
                # every wx app needs an app
                self.app = wx.App()
                # create the top-level frame, sizer and wxVTKRWI
                self.frame = wx.Frame(None, -1, "VTK RenderWindow", size=wx.Size(400,400))
                self.widget = wxVTKRenderWindow(self.frame, -1)
                self.sizer = wx.BoxSizer(wx.VERTICAL)
                self.sizer.Add(self.widget, 1, wx.EXPAND)
                self.frame.SetSizer(self.sizer)
                self.frame.Layout()
                self.widget.Enable(1)
                self.widget.AddObserver("ExitEvent", lambda o,e,f=self.frame: f.Close())
                self.renWin=self.widget.GetRenderWindow()
                self.renWin.AddRenderer(self.ren)
                self.iren=self.widget._Iren
                self.frame.Show(1)
            else:
                self.renWin = vtk.vtkRenderWindow()
                self.renWin.AddRenderer(self.ren)
                self.renWin.AddObserver("AbortCheckEvent", self.CheckAbort)
                self.iren = vtk.vtkRenderWindowInteractor()
                self.iren.SetRenderWindow(self.renWin)
    
    def SetExternalRenWin(renWin):
        self.renWin=renWin
        self.SkipRender=False
    
    def SetColorRange(self,cmin,cmax):
        self.SetColormap(None,cmin,cmax)
    
    def SetNumpyArray(self,dat,scale=[1,1,1],center=True):
        self.inputArr=n.array(dat)
        
        typecodeHash={'c':vtk.vtkCharArray, 'b':vtk.vtkCharArray, 'h':vtk.vtkShortArray,
                      'l':vtk.vtkLongArray, 'B':vtk.vtkUnsignedCharArray, 'H':vtk.vtkUnsignedShortArray,
                      'L':vtk.vtkUnsignedLongArray, 'f':vtk.vtkFloatArray, 'd':vtk.vtkDoubleArray
                     }
        
        SetTypecodeHash={'c':self.imgdat.SetScalarTypeToChar, 'b':self.imgdat.SetScalarTypeToChar,
                         'h':self.imgdat.SetScalarTypeToShort, 'l':self.imgdat.SetScalarTypeToLong,
                         'B':self.imgdat.SetScalarTypeToUnsignedChar, 'H':self.imgdat.SetScalarTypeToUnsignedShort,
                         'L':self.imgdat.SetScalarTypeToUnsignedLong, 'f':self.imgdat.SetScalarTypeToFloat,
                         'd':self.imgdat.SetScalarTypeToDouble
                        }
        
        try:
            self.vArr=typecodeHash[self.inputArr.dtype.char]()
            SetTypecodeHash[self.inputArr.dtype.char]()
            # Index the dictionary (hashes) and call the function in the hash
        except KeyError:
            print("Unknown Bytecode Passed to VTKPlotObject!!!")
            print("Defaulting to Float")
            self.vArr=typecodeHash['f']()
        
        [self.NX,self.NY]=self.inputArr.shape[:2]
        
        if len(self.inputArr.shape)>2:
            self.NZ=self.inputArr.shape[2]
        else:
            self.NZ=1
        
        self.vArr.SetVoidArray(self.inputArr,self.inputArr.size,1)
        self.imgdat.GetPointData().SetScalars(self.vArr)
        
        if len(self.inputArr.shape)>2:
            self.imgdat.SetDimensions(self.NZ, self.NY, self.NX) #set dimensions as necessary
        else:
            self.imgdat.SetDimensions(self.NY, self.NX, self.NZ) #set dimensions as necessary
        
        SetTypecodeHash[self.inputArr.dtype.char]()
        
        if(center==True):
            if len(self.inputArr.shape)>2:
                self.imgdat.SetOrigin(0.5*self.NY,0.5*self.NX,0.5*self.NZ)
            else:
                self.imgdat.SetOrigin(0.5*self.NY,0.5*self.NX,0)
        else:
            self.imgdat.SetOrigin(0,0,0)
        
        self.imgdat.SetSpacing(1, 1, 1) #set spacing as necessary
        self.res.SetResliceAxesDirectionCosines((1./scale[0],0,0),(0,1./scale[1],0),(0,0,1./scale[2]))
    def SetColormap(self,cmap=None,cmin=-1,cmax=1,lutSize=100):
        try:
            cmap=cmap._segmentdata # if we passed a matplotlib (pylab) cmap, grab the real data
        except:
            pass
        
        # Default to jet for the cmap
        if cmap==None:
            if self.cmap==None:
                cmap={'blue': ((0.0, 0.5, 0.5), (0.11, 1, 1), (0.34000000000000002, 1, 1), (0.65000000000000002, 0, 0), (1, 0, 0)),
                      'green': ((0.0, 0, 0), (0.125, 0, 0), (0.375, 1, 1), (0.64000000000000001, 1, 1), (0.91000000000000003, 0, 0), (1, 0, 0)),
                      'red': ((0.0, 0, 0), (0.34999999999999998, 0, 0), (0.66000000000000003, 1, 1), (0.89000000000000001, 1, 1), (1, 0.5, 0.5))}
                self.cmapSet=True
            # Otherwise use a previously defined map in self.cmap
            # This lets SetColormaps double as a "SetColorRange" function, not trivial since the ctf is so loopy...
        
        # Create a contiguous block of data for VTK to use:
        [r,g,b]=[cmap['red'],cmap['green'],cmap['blue']]
        s=set()
        for i in [r,g,b]:
            for j in i:
                s.add(j[0]-1e-10)
                s.add(j[0])
        xvals=list(s)
        xvals.sort()
        self.xrgb=[]
        self.ctf.RemoveAllPoints()
        for i in xvals:
            self.xrgb.append([cmin+i*(cmax-cmin),PW(i,r),PW(i,g),PW(i,b)])
            self.ctf.AddRGBPoint(*self.xrgb[-1]) # the asteric here means "apply", basically removing the list structure of self.xrgb[-1]
            
        
        # Create a LUT from the CTF
        f=vtk.vtkFloatArray()
        a=n.arange(cmin,cmax+1.*(cmax-cmin)/lutSize,1.*(cmax-cmin)/lutSize,dtype=n.float32)
        
        f.SetVoidArray(a,a.size,1)
        
        self.lutScalars=self.ctf.MapScalars(f,0,0)
        
        self.lut.SetTable(self.lutScalars)
        self.lut.SetTableRange(cmin,cmax)
    def SaveImage(self,fileName="out.jpg",quality=100):
        w2if=vtk.vtkWindowToImageFilter()
        w2if.SetInput(self.renWin)
        
        writer=vtk.vtkJPEGWriter()
        writer.SetInput(w2if.GetOutput())
        writer.SetQuality(quality)
        writer.SetFileName(fileName)
        writer.ProgressiveOff()
        writer.Write()
    
    def UpdateState(self,newState):
        if self.state==newState:
            return # Nothing needs to change
        
        # Remove any actors that shouldn't be here
        if self.state=='none':
            pass
        elif self.state=='scatter':
            self.ren.RemoveActor(self.glyph)
        elif self.state=='image':
            self.ren.RemoveActor(self.actor2D)
        elif self.state=='surf':
            self.ren.RemoveActor(self.actor)
        elif self.state=='volume':
            self.ren.RemoveVolume(self.volume)
        else:
            print 'Invalid State!  Something has gone haywire...'
            return
        
        # If old state was valid and newState is different, set current state to "none"
        self.state='none'
        
        # Add the new actor
        if newState=='none':
            return
        elif newState=='scatter':
            self.ren.AddActor(self.glyph)
        elif newState=='image':
            self.ren.AddActor(self.actor2D)
        elif newState=='surf':
            self.ren.AddActor(self.actor)
        elif newState=='volume':
            self.ren.AddVolume(self.volume)
        else:
            print 'Invalid New State passed!  State now set to "none"'
            return
        
        # If new state was valid and newState is different, set current state to newState
        self.state=newState
        return
    
    def Scatter3D(self,dat,scale=[1,1,1], center=True,resetCamera=True,ballRadius=0.01,ballPhiRes=10,ballThetaRes=10,color=tomato,specular=0.3,specularPower=30):
        self.UpdateState('scatter')
        
        self.inputArr=n.array(dat,n.float32)
        ##vArr.SetVoidArray(inputArr,inputArr.size,1)
        ##vArr.SetNumberOfTuples(10)
        ##inputPoints.SetData(vArr) # How should I be doing this???
        # Until the above code can be made to work, best to just do this the slow way...
        # It's certainly simple enough!!
        self.inputPoints=vtk.vtkPoints()
        for i in self.inputArr:
            self.inputPoints.InsertNextPoint(i)
        
        self.inputData.SetPoints(self.inputPoints)
        
        # Set Misc. Properties:
        self.color=color
        self.balls.SetRadius(ballRadius)
        self.balls.SetPhiResolution(ballPhiRes)
        self.balls.SetThetaResolution(ballThetaRes)
        self.glyph.GetProperty().SetDiffuseColor(self.color)
        self.glyph.GetProperty().SetSpecular(specular)
        self.glyph.GetProperty().SetSpecularPower(specularPower)
        
        # These are not needed here...
        ##self.SetNumpyArray(dat,scale,center)
        ##self.imap2c.SetLookupTable(self.lut)
        
        self.warp.SetScaleFactor(scale[2])
        if not self.SkipRender:
            if self.useWx:
                self.widget.Render()
            else:
                self.renWin.Render()
        if(resetCamera):
            self.ren.ResetCamera()
        
    def ImShow(self,dat,scale=[1,1], center=True,windowSize=[None,None]): # If window is specified, stretch to fit using scale
        self.UpdateState('image')
        
        if windowSize!=[None,None]:
            scale=list(1.*n.r_[windowSize]/n.r_[dat.shape])
        self.SetNumpyArray(dat,scale+[1],center)
        
        self.imap2c.SetLookupTable(self.lut)
        
        if not self.SkipRender:
            if windowSize==[None,None]:
                self.renWin.SetSize(n.int32(n.r_[dat.shape][::-1]*n.r_[scale]))
            else:
                self.renWin.SetSize(windowSize)
            if self.useWx:
                self.widget.Render()
            else:
                self.renWin.Render()
    def Surf(self,dat,scale=[1,1,1], center=True,resetCamera=True):
        self.UpdateState('surf')
        
        self.SetNumpyArray(dat,scale,center)
        self.imap2c.SetLookupTable(self.lut)
        self.warp.SetScaleFactor(scale[2])
        if not self.SkipRender:
            if self.useWx:
                self.widget.Render()
            else:
                self.renWin.Render()
        if(resetCamera):
            self.ren.ResetCamera()
    def Volume3D(self,dat,scale=[1,1,1], center=True,resetCamera=True):
        self.UpdateState('volume')
        
        self.dat=n.array(dat,n.uint16) #Unsist on unsigned short
        self.SetNumpyArray(self.dat,scale,center)
        self.volume.SetScale(*scale) # The star here means "apply" the function to "scale"
        self.pts.GetPointData().SetScalars(self.vArr)
        self.pts.SetDimensions(dat.shape[2],dat.shape[1],dat.shape[0]) #set dimensions as necessary
        
        if not self.SkipRender:
            if self.useWx:
                self.widget.Render()
            else:
                self.renWin.Render()
        if(resetCamera):
            self.ren.ResetCamera()
        self.state='volume'
        
    def SetVolumeOpacity(self,dataRange=[0,1],opacity=[0,0.1]):
        self.opacityTransferFunction.RemoveAllPoints()
        if len(dataRange)!=len(opacity):
            print "Error! -- dataRange and Opacity must have the same number of elements"
        
        for i in range(min(len(dataRange),len(opacity))):
            self.opacityTransferFunction.AddPoint(dataRange[i], opacity[i])
            #print 'dataRange,opacity',dataRange[i],opacity[i]
        
    def StartInteractive(self,resetCamera=True):
        if not self.SkipRender: # Start manually if using external renWin
            if self.useWx:
                self.widget.Render()
            else:
                self.renWin.Render()
            if(resetCamera):
                self.ren.ResetCamera()
            
            if self.useWx:
                self.app.MainLoop()
            else:
                self.iren.Initialize()
                self.iren.Start()
    def CheckAbort(self,obj, event):
        if obj.GetEventPending() != 0:
            obj.SetAbortRender(1)

VTK3DPlotObject=VTK2DPlotObject=VTKPlotObject #Legacy Names...


#Use underscores to indicate "hidden" variables:
_FigureHash={}
#For some reason, I had to use a singular valued list to make it mutable later...
_Figure=[0]

def GetCurrentFigure():
    SetFigure(_Figure[0])
    return _FigureHash[_Figure[0]]

def GetFigure(number):
    SetFigure(number)
    return _FigureHash[number]

def SetFigure(number,useExternalRenWin=False,renWin=None,useWx=False):
    # if the figure already exists, this will not overwrite it or change the render window
    try:
        _FigureHash[number]
    except: # if this index doesn't exist, make a new one
        _FigureHash[number]=VTKPlotObject(useExternalRenWin=useExternalRenWin,renWin=renWin,useWx=useWx)
    
    _Figure[0]=number

def Scatter3D(dat,scale=[1,1,1], center=True,resetCamera=True,ballRadius=0.01,ballPhiRes=10,ballThetaRes=10,color=tomato,specular=0.3,specularPower=30,figure=None):
    if figure!=None: # Here figure should be passed as a key; later it is used as the actual VTKInterface object
        if figure!=_Figure[0]:
            SetFigure(figure,useExternalRenWin=False)
    
    SetFigure(_Figure[0])
    figure=GetCurrentFigure()
    figure.Scatter3D(dat,scale=scale, center=center,resetCamera=resetCamera,ballRadius=ballRadius,ballPhiRes=ballPhiRes,ballThetaRes=ballThetaRes,color=color,specular=specular,specularPower=specularPower)
    

def Surf2D(dat,cmap=None,cmin=-10,cmax=10,scale=[1,1],center=True,figure=None,windowSize=[None,None]): # If window is specified, stretch to fit using scale
    if figure!=None: # Here figure should be passed as a key; later it is used as the actual VTKInterface object
        if figure!=_Figure[0]:
            SetFigure(figure,useExternalRenWin=False)
    
    SetFigure(_Figure[0])
    figure=GetCurrentFigure()
    figure.SetColormap(cmap,cmin,cmax)
    figure.ImShow(dat,scale=scale,center=center,windowSize=windowSize)

def Surf3DFlat(dat,cmap=None,cmin=-10,cmax=10,scale=[1,1,1],center=True,resetCamera=True,figure=None):
    if figure!=None: # Here figure should be passed as a key; later it is used as the actual VTKInterface object
        if figure!=_Figure[0]:
            SetFigure(figure,useExternalRenWin=False)
    
    scale=[scale[0],scale[1],1e-10]
    SetFigure(_Figure[0])
    figure=GetCurrentFigure()
    figure.SetColormap(cmap,cmin,cmax)
    figure.Surf(dat,scale=scale,center=center,resetCamera=resetCamera)

def Surf3D(dat,cmap=None,cmin=-10,cmax=10,scale=[1,1,1],center=True,resetCamera=True,figure=None):
    if figure!=None: # Here figure should be passed as a key; later it is used as the actual VTKInterface object
        if figure!=_Figure[0]:
            SetFigure(figure,useExternalRenWin=False)
    print 1
    SetFigure(_Figure[0])
    print 2
    figure=GetCurrentFigure()
    print 3
    figure.SetColormap(cmap,cmin,cmax)
    print 4
    figure.Surf(dat,scale=scale,center=center,resetCamera=resetCamera)
    print 5

def Volume3D(dat,cmap=None,cmin=-10,cmax=10,opacity=[0,.5],scale=[1,1,1],center=True,resetCamera=True,figure=None):
    if figure!=None: # Here figure should be passed as a key; later it is used as the actual VTKInterface object
        if figure!=_Figure[0]:
            SetFigure(figure,useExternalRenWin=False)
    
    SetFigure(_Figure[0])
    figure=GetCurrentFigure()
    
    opacity=n.array(opacity)
    
    # This lets the user also pass a single number for opacity...
    if len(opacity.shape)==0:
        opacity=n.array([opacity,opacity])
    elif len(opacity)==1:
        opacity=n.array([opacity[0],opacity[0]])
    
    #This allows the user to pass a 0,1,or 2D array (or list) for opacity
    if len(opacity.shape)==1:
        opacity=[n.arange(cmin,cmax+1.*(cmax-cmin)/len(opacity),1.*(cmax-cmin)/(len(opacity)-1)),opacity]
    elif len(opacity.shape)==2:
        if opacity.shape[1]!=2:
            print "opacity should be given as a 0D, 1D, or Nx2 array!"
            return
        opacity=opacity.T
    else:
        print "opacity should be given as a 0D, 1D, or Nx2 array!"
        return
    
    figure.SetColormap(cmap,cmin,cmax)
    figure.SetVolumeOpacity(opacity[0],opacity[1])
    figure.Volume3D(dat,scale=scale,center=center,resetCamera=resetCamera)

def Start3DGUI(figure=None):
    if figure!=None: # Here figure should be passed as a key; later it is used as the actual VTKInterface object
        if figure!=_Figure[0]:
            SetFigure(figure,useExternalRenWin=False)
    
    SetFigure(_Figure[0])
    GetCurrentFigure().StartInteractive()

def Surf2DMovie(dat,t_axis=0,start=0,stop=-1,step=1,border=True,useGauss=True,gaussStd=1,cmap=None,cmin=-10,cmax=10,scale=[1,1],center=True,figure=None):
    "Automatically play a 2D movie from a 3D data set"
    if figure!=None: # Here figure should be passed as a key; later it is used as the actual VTKInterface object
        if figure!=_Figure[0]:
            SetFigure(figure,useExternalRenWin=False)
    
    SetFigure(_Figure[0])
    
    dat=n.array(dat)
    
    s=dat.shape
    if len(s)!=3:
        print "Must pass a 3D dataset!"
        return
    
    if border:
        r=range(3)
        r.remove(t_axis)
        z=n.zeros([s[r[0]]+2,s[r[1]]+2],dtype=dat.dtype)
        z[0,0]=100
        z[0,-1]=100
        z[-1,0]=100
        z[-1,-1]=100
    
    for i in range(s[t_axis]):
        if t_axis==0:    d=dat[i,:,:]
        elif t_axis==1:  d=dat[:,i,:]
        elif t_axis==2:  d=dat[:,:,i]
        
        if useGauss:  g=gauss(d,gaussStd)
        else:       g=d
        
        if border:  z[1:-1,1:-1]=g
        else:       z=g
        
        Surf2D(z,cmap,cmin,cmax,scale,center)



def Surf3DMovie(dat,t_axis=0,start=0,stop=-1,step=1,border=True,useGauss=True,gaussStd=1,cmap=None,cmin=-10,cmax=10,scale=[1,1,1],center=True,resetCamera=False,figure=None):
    "Automatically play a 3D surface movie from a 3D data set"
    
    if figure!=None: # Here figure should be passed as a key; later it is used as the actual VTKInterface object
        if figure!=_Figure[0]:
            SetFigure(figure,useExternalRenWin=False)
    
    SetFigure(_Figure[0])
    
    dat=n.array(dat)
    
    s=dat.shape
    if len(s)!=3:
        print "Must pass a 3D dataset!"
        return
    
    if border:
        r=range(3)
        r.remove(t_axis)
        z=n.zeros([s[r[0]]+2,s[r[1]]+2],dtype=dat.dtype)
        z[0,0]=100
        z[0,-1]=100
        z[-1,0]=100
        z[-1,-1]=100
    
    for i in range(s[t_axis]):
        if t_axis==0:    d=dat[i,:,:]
        elif t_axis==1:  d=dat[:,i,:]
        elif t_axis==2:  d=dat[:,:,i]
        
        if useGauss:  g=gauss(d,gaussStd)
        else:       g=d
        
        if border:  z[1:-1,1:-1]=g
        else:       z=g
        
        Surf3D(z,cmap,cmin,cmax,scale,center,resetCamera)


def Volume3DMovie(dat,t_axis=0,start=0,stop=-1,step=1,border=True,useGauss=True,gaussStd=1,cmap=None,cmin=-10,cmax=10,opacity=[0,.5],scale=[1,1,1],center=True,resetCamera=True,figure=None):
    "Automatically play a 3D volume movie from a 4D data set"
    
    if figure!=None: # Here figure should be passed as a key; later it is used as the actual VTKInterface object
        if figure!=_Figure[0]:
            SetFigure(figure,useExternalRenWin=False)
    
    SetFigure(_Figure[0])
    
    dat=n.array(dat)
    
    s=dat.shape
    if len(s)!=4:
        print "Must pass a 4D dataset!"
        return
    
    for i in range(s[t_axis]):
        if t_axis==0:    d=dat[i,:,:,:]
        elif t_axis==1:  d=dat[:,i,:,:]
        elif t_axis==2:  d=dat[:,:,i,:]
        elif t_axis==3:  d=dat[:,:,:,i]
        
        if useGauss:  g=gauss(d,gaussStd)
        else:       g=d
        
        Volume3D(g,cmap,cmin,cmax,opacity,scale,center,resetCamera)


if __name__=="__main__":
    #Arr=10*n.array([[1,2,3,4]*10,[2,3,4,5]*10,[3,4,5,6]*10,[4,5,6,7]*10,[7,8,9,10]*10]*10,dtype=n.float32)-100
    #Arr=10*n.array([[1,2,3,4],[2,3,4,5],[3,4,5,6],[4,5,6,7],[7,8,9,10]],dtype=n.float32)
    #Arr=n.array([[-85.5,-85.5]*120,[20,20]*120]*300,dtype=n.float32)
    Arr=n.array([-85.5*n.ones([300,800],dtype=n.float32),20*n.ones([300,800],dtype=n.float32)],dtype=n.float32)
    Arr=Arr.reshape(600,800)
    myArr=n.array(Arr,dtype=n.float32)
    
    v=VTKPlotObject(cmin=-1,cmax=1,useWx=False)
    v2=VTKPlotObject(cmin=-1,cmax=1,useWx=False)
    v.Surf(myArr)
    
    #vI.SurfStart()
    
    import time
    c=time.clock()
    
    x=n.arange(200)
    y=n.arange(200)
    myArr=n.array(0/10.*n.outer(n.sin(x*3*n.pi/100),n.cos(y*3*n.pi/100)),n.float32)
    v.ImShow(myArr)
    v2.Surf(myArr,resetCamera=False,scale=[1,1,10])
    v2.StartInteractive()
    
    import wx
    app=wx.App(0);app.MainLoop()
    w=wx.MessageDialog(None,'''Close this window,
adjust the surf plot with the mouse,
then push the close button on the VTK surf window to continue...''')
    w.ShowModal()
    for g in range(0,100,10):
        #a=n.array([-85.5*n.ones([200,240],dtype=n.float32),20*n.ones([200,240],dtype=n.float32),20*n.ones([200,240],dtype=n.float32)],dtype=n.float32)
        #myArr=n.ones([550-g,int(220-g/2.5)],n.float32)
        #myArr[300:(300+g),:]=g
        #myArr[0:20]=-20
        x=n.arange(200)
        y=n.arange(200)
        myArr=n.array(g/10.*n.outer(n.sin(x*3*n.pi/100),n.cos(y*3*n.pi/100)),n.float32)
        v.SetColorRange(-10,10)
        v.ImShow(myArr)
        v2.Surf(myArr,resetCamera=False,scale=[1,1,10])
        v.SaveImage(os.path.expanduser('~/Desktop/outI.jpg'))
        v2.SaveImage(os.path.expanduser('~/Desktop/outS.jpg'))
    
    dat=n.ones([100,100,100],n.uint16)*100
    dat[:]=n.arange(100)*2
    dat[50:100,50:100,50:100]=50
    v2.SetVolumeOpacity([0,255],[0,0.2])
    v2.SetColorRange(0,200)
    v2.Volume3D(dat)
    v2.StartInteractive()
    
    print time.clock()-c
    
#    vI.SurfStart()
    wx.MessageBox('''Close this window to end the program''')
