import optparse
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import sfepy.fem

def PlotQuadrature(quadType,order):
    qp=sfepy.fem.quadratures.quadrature_tables[quadType][order]
    c=qp.coors
    w=qp.weights
    
    fig = plt.figure()
    plt.clf()
    if quadType[0]=='3':
        ax = fig.add_subplot(111, projection='3d')

    if quadType=='1_2':
        plt.plot(qp.bounds,[0,0])
        for i in range(len(c)):
            plt.plot(c[i],0,'k.',ms=w[i]*100)
    elif quadType=='2_3':
        plt.plot([0,1,0,0],[0,0,1,0])
        for i in range(len(c)):
            plt.plot(c[i][0],c[i][1],'k.',ms=w[i]*100)
    elif quadType=='2_4':
        plt.plot([0,1,1,0,0],[0,0,1,1,0])
        for i in range(len(c)):
            plt.plot(c[i][0],c[i][1],'k.',ms=w[i]*100)
    elif quadType=='3_4':
        ax.plot([0,0,0], [1,0,0], [0,0,1], color='b')
        ax.plot([0,1,0,0,1], [0,0,1,0,0], [0,0,0,1,0], color='b')
        for i in range(len(c)):
            print [c[i][0]],[c[i][1]],[c[i][2]],w[i]
            ax.scatter([c[i][0]],[c[i][1]],[c[i][2]],s=[w[i]*1000],color='r')
    elif quadType=='3_8':
        pts = [[i,j,k] for i in [0,1] for j in [0,1] for k in [0,1]]
        lines = [[i,j] for i in pts for j in pts if (pts.index(i)<pts.index(j) and sum(abs(np.array(i)-j))==1)]
        for l in lines:
            l=np.transpose(l)
            ax.plot(l[0], l[1], l[2], color='b')
        for i in range(len(c)):
            print [c[i][0]],[c[i][1]],[c[i][2]],w[i]
            ax.scatter([c[i][0]],[c[i][1]],[c[i][2]],s=[w[i]*1000],color='r')

if __name__=='__main__':
    qt = sfepy.fem.quadratures.quadrature_tables
    optsString=''
    for i in qt.keys():
        optsString=optsString+str(i)+str([j for j in qt[i].keys()])+'\n'
    
    usage = """%prog quadType order
    Plot one of the quadratures in sfepy. Possibilities include:\n""" + optsString
    
    parser = optparse.OptionParser(usage=usage, version="%prog " + sfepy.__version__)
    
    options, args = parser.parse_args()
    print args
    #defaults
    quadType='3_4'
    order=4
    
    if len(args)==1:
        print 'You must provide both quadType and order'
        print usage
        exit()
    if len(args)>1:
        quadType=args[0]
        order = int(args[1])
    if quadType not in qt.keys():
        print 'quadType must be one of:', qt.keys()
        exit()
    
    
    PlotQuadrature(quadType,order)
    
    import DelayApp
