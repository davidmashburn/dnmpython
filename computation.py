import os
import json
import numpy as np

from np_utils import kwdPop

class Computation(object):
    '''Abstract base class for a saving/auto-loading computation
       Uses repr and eval as an over-simplistic save/load
       
       Other abstract classes will define save and load
       
       All real classes must define "call"'''
    def __call__(self,*args,**kwds):
        saveFile = kwdPop(kwds,'saveFile',None)
        resetFile = kwdPop(kwds,'resetFile',False)
        if self.test(saveFile) and not resetFile:
            return self.load(saveFile)
        else:
            r = self.call(*args,**kwds)
            if saveFile:
                self.save(r,saveFile)
            return r
    
    def call(self):
        return None
    
    def test(self,f):
        if f:
            return os.path.exists(f)
        else:
            return False
    
    def save(self,x,f):
        with open(f,'w') as fid:
            fid.write(repr(x))
    
    def load(self,f):
        with open(f,'r') as fid:
            return eval(fid.read())
    
    def clear(self,f):
        os.remove(f)

class ComputationJson(Computation):
    '''A better solution for save/load on simple data'''
    def save(self,x,f):
        with open(f,'w') as fid:
            json.dump(x,fid)
    
    def load(self,f):
        with open(f,'r') as fid:
            return json.load(fid)

if __name__=='__main__':
    class MultiplyBy10(ComputationJson):
        def call(self,x):
            print 'Run',x
            return 10*x
    m10 = MultiplyBy10()
    
    testVals = range(3)
    files = [ '/tmp/out%i.json' % i for i in testVals ]
    
    print 'Clear any pre-existing save files for this test'
    for f in files:
        os.remove(f)
    
    print 'Normal all, no saving:'
    print [ m10(i) for i in testVals ]
    
    print 'Call with save/load enabled'
    print [ m10(i,saveFile=f) for i,f in zip(testVals,files) ]
    
    print 'Call with save/load enabled'
    print [ m10(i,saveFile=f) for i,f in zip(testVals,files) ]
    
    print 'Call with reset enabled'
    print [ m10(i,saveFile=f,resetFile=True) for i,f in zip(testVals,files) ]
    
    print 'Call with save/load enabled'
    print [ m10(i,saveFile=f) for i,f in zip(testVals,files) ]
    
    
