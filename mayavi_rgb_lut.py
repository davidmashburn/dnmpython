'''Utilities to specify the color of points3d by element rgb
(instead of using scalar values and a colormap)
Adapted from:
https://stackoverflow.com/a/28681836/2344211
'''
import numpy as np
import mayavi.mlab

RGB_LUT_CONTAINER = []

#create direct grid as 256**3 x 4 array 
def create_8bit_rgb_lut():
    xl = np.mgrid[0:256, 0:256, 0:256]
    size = 256 ** 3
    lut = np.vstack((xl[0].reshape(1, size),
                     xl[1].reshape(1, size),
                     xl[2].reshape(1, size),
                     255 * np.ones((1, size)))).T
    return lut.astype('int32')

def store_8bit_rgb_lut():
    '''Create and store the large 8bit color LUT in RGB_LUT
    (but only if it doesn't exist)
    '''
    if not RGB_LUT_CONTAINER:
        RGB_LUT_CONTAINER.append(create_8bit_rgb_lut())

# indexing function to above grid
def rgb_2_scalar_idx(r, g, b):
    return 65536 * r + 256 * g + b

def points3d_rgb(x, y, z, colors, **kwds):
    '''Plot a series of points with color specified
    
    This implementation is very generic, but the problem is the lut
    is HUGE (16M colors, 2**24)
    
    To speed it up, call store_8bit_rgb_lut() ahead of time.
    
    colors is a required argument (or else you should be using points3d)
    sizes is an optional argument - it specifies scalars to use as
    the point sizes (default 1)
    '''
    store_8bit_rgb_lut()
    rgb_lut = RGB_LUT_CONTAINER[0]
    sizes = (kwds.pop('sizes') if 'sizes' in kwds else
             np.ones_like(x))
    
    color_scalars = rgb_2_scalar_idx(colors[:, 0],
                                     colors[:, 1],
                                     colors[:, 2])

    pts = mayavi.mlab.quiver3d(x, y, z, sizes, sizes, sizes,
                               scalars=color_scalars,
                               mode="sphere",
                               **kwds)
    
    pts.glyph.color_mode = "color_by_scalar"
    pts.glyph.glyph_source.glyph_source.center = [0,0,0]
    
    #magic to modify lookup table 
    pts.module_manager.scalar_lut_manager.lut._vtk_obj.SetTableRange(0, rgb_lut.shape[0])
    pts.module_manager.scalar_lut_manager.lut.number_of_colors = rgb_lut.shape[0]
    pts.module_manager.scalar_lut_manager.lut.table = rgb_lut
    
    return pts

def color_space_points(colors, point_size=2):
    '''Plot a series of colors as colored 3d points in rgb space'''
    x, y, z = colors.T
    return points3d_rgb(x, y, z, colors, scale_factor=point_size)

if __name__ == '__main__':
    n = 200
    colors = np.random.randint(255, size=(n, 3))
    color_space_points(colors)
    mayavi.mlab.show()

    
