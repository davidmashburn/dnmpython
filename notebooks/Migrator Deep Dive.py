#!/usr/bin/env python
# coding: utf-8

# This is a deep dive to explain the different parts of the new migrator bash script in scoring-performance.
# 
# If you want some bash basics, there are lots of bash cheat sheets like this one: https://devhints.io/bash
# 
# This doc is organized as explanation block followed by code block.

# Setup, comment at the top:

# In[ ]:


#!/usr/bin/env bash

# A really simple migration manager for PostgreSQL.
# Useful as an add-on to a project repo that uses postgres
# This is a stripped down version of dogfish
# by Dan Brown <dan@stompydan.net>
# https://github.com/dwb/dogfish


# Flag to stop immediately on error (makes bash act more like Python):

# In[ ]:


set -e


# Temporary way to set up DB creds:

# In[ ]:


# These should be set as environment variables or hard-coded here:
HOST="localhost"
USER="postgres"
PASSWORD="postgres"

PORT="5432"
DBNAME="postgres"
CREDS="host=$HOST user=$USER password=$PASSWORD port=$PORT dbname=$DBNAME"


# Set some global variables

# In[ ]:


schema_migrations_table="schema_migrations"
migration_id_column="migration_id"
migrations_dir="migrations"


# Standard argument handling in bash. The only option really left is "-h".
# 
# First look for known flags and break once an argument is hit that does not start with "-".
# 
# `${1-}` means unpack the first argument but don't fail if the value is missing.
# https://stackoverflow.com/questions/32674713/what-does-the-dash-after-variable-names-do-here/32674802

# In[ ]:


while true; do
  case ${1-} in
    "-h" | "--help")
      while read line; do printf '%s\n' "$line"; done <<END
migrator

usage: migrator migrate|rollback [FINISH_AT_MIGRATION]
       migrator remigrate
       migrator create-migration [MIGRATION_NAME]
       migrator list

      finish_at_migration is the (optional) number of the migration to finish
          processing after
      migration_name is an optional description of the migration

'remigrate' rolls back and re-applies the last migration. Useful for
development.

Commands are sent to the database using unassuming calls to psql.
Hostname and credentials are set using environment variables.

The SQL scripts themselves are named "migrate-version-name.sql" or
"rollback-version-name.sql", where version is the numeric version number
(usually an ISO YMDHms timestamp, without punctuation), and name is whatever
you want. If you don't provide a rollback script for a particular version, no
complaining will happen. You can also provide a rollback script with no migrate
companion if you're feeling really wild.
END
      exit
      (";")
    -*)
      echo -e "unrecognised option '$1'"
      exit 1
      (";")
    *)
      break 2
      (";")
  esac
done


# Handle the non-flag arguments, round 1 (round 2 happens at the end)
# 
# `shift` moves on to the next argument so what was `${2}` is now `${1}`.
# https://unix.stackexchange.com/questions/174566/what-is-the-purpose-of-using-shift-in-shell-scripts
# 
# I think `|| true` is a construct to prevent errors, but I'm not really sure... if it ain't broke don't fix it :)

# In[ ]:


action=${1-}; shift || true
case $action in
  migrate|rollback|remigrate)
    finish_at_version=${1-}; shift || true
    (";")
  create-migration)
    migration_name=${1-}; shift || true
    (";")
  list)
    (";")
esac


# Stop if one of the known actions is not given and offer helpful feedback as a message.

# In[ ]:


if [[ -z $action ]]; then
  echo -e "Action not given. Use one of:\n
  migrator list
  migrator migrate\n
  migrator rollback\n
  migrator remigrate\n
  migrator create-migration"
  exit 1
fi


# Stop if the migrations directory is missing.

# In[ ]:


if ! [[ -d ${migrations_dir} ]]; then
  echo -e "Migrations directory ${migrations_dir} not found"
  exit 1
fi


# Find the migrations (action is a global and will be "migration" or "rollback") within the "migrations" folder.
# 
# pushd and popd is a more convenient way to manage changing directories than cd
# https://www.tecmint.com/pushd-and-popd-linux-filesystem-navigation/
# 
# sed is essentially a "stream editor" with regex powers for the command line.
# https://www.gnu.org/software/sed/manual/sed.html
# 
# This pattern is the "duplicate" pattern, "sed 's/unix/linux/p' geekfile.txt" as explained here:
# https://www.geeksforgeeks.org/sed-command-in-linux-unix-with-examples/
# 
# This might also help:
# https://explainshell.com/explain?cmd=ls+%7C+sed+-ne+%22s%2F%5E%24%7Baction%7D-%5C%28%5B%5B%3Adigit%3A%5D%5D%5C%7B1%2C%5C%7D%5C%29%5B-a-zA-Z%5D*%5C.sql%24%2F%5C1+%26%2Fp%22
# 
# As usual, regex is black magic. This is a version of the regex you can drop into an online regex explainer like regexr.org:
# 
# ^migrate-\([[:digit:]]\{1,\}\)[-a-zA-Z]*\.sql$
# 
# This is a sample output from this function, given these are the files in the migrations folder and action=migrate:
# * migrate-20200306033831-intial-migration.sql
# * migrate-20200306034031-add-column-to-table.sql
# * rollback-20200306033831-intial-migration.sql
# * rollback-20200306034031-add-column-to-table.sql
# 
# 20200306033831 migrate-20200306033831-intial-migration.sql
# 
# 20200306034031 migrate-20200306034031-add-column-to-table.sql

# In[ ]:


function available_migrations_to_scripts() {
  set -e
  pushd "${migrations_dir}" >/dev/null
  # TODO: work out how to not use `ls` here: won't deal with newlines in
  # file names and all that classic stuff. But then the regex will filter
  # out any weirdness, so not that bad.
  #
  # Quieten shellcheck for this one, we know about it:
  # shellcheck disable=SC2012
  ls | sed -ne "s/^${action}-\([[:digit:]]\{1,\}\)[-a-zA-Z]*\.sql$/\1 &/p"
  popd >/dev/null
}


# functions that create the set of migrations using the above function
# 
# awk '{print $1}' gets the first element of each line (separated by whitespace)
# 
# So available_migrations gives:
# 
# 20200306033831
# 
# 20200306034031

# In[ ]:


function available_migrations() {
  available_migrations_to_scripts | awk '{print $1}'
}


# available_migration_script_for_id takes an id (entry from the output of line from available_migrations) and returns the filename:
# 
# available_migration_script_for_id 20200306033831
# 
# -->
# 
# migrate-20200306033831-intial-migration.sql
# 
# This works by using `egrep` (search tool) with the flag `-m1` (stop after 1 word is found) with the argument `^$1\>` which says look for something that starts with the input (`^` follwed by `$1`) and is a whole word/ ends with a space (`\>`)
# 
# `awk '{print $2}'` selects the second word

# In[ ]:


function available_migration_script_for_id() {
  available_migrations_to_scripts | egrep -m1 "^$1\>" | awk '{print $2}'
}


# Wrapper around psql, automatically passes in creds. Not sure about all the options, but if it ain't broke...
# 
# return $? just sets the output of this command to the output from the last command, not really sure why it is used here.

# In[ ]:


function call_psql() {
  psql "${CREDS}" --no-psqlrc --single-transaction --quiet --tuples-only --no-align --no-password
  return $?
}


# Query the `schema_migrations` table for all the previously run migrations.
# 
# 
# `<<XXX  ....  XXX` is a construct for wrapping (multiple lines of) text as a "file" so it can be passed to a command that reads a file (like `cat`).
# 
# Ex:
# 
#     cat <<END
#     Hi.
#     ho.
#     END
# 
# Prints
#           
#     Hi.
#     ho.

# In[ ]:


function applied_migrations() {
  set -e
  call_psql <<END
  SELECT ${migration_id_column}
  FROM ${schema_migrations_table}
  ORDER BY ${migration_id_column} ASC;
END
}


# Store the migration or rollback information in the schema_migrations table (to be used after the migration is completed).
# 
# INSERT after a migration, DELETE after a rollback.

# In[ ]:


function post_apply_sql() {
  if [[ $action == "migrate" ]]; then
    echo "INSERT INTO \"${schema_migrations_table}\" (\"${migration_id_column}\") VALUES ('$1');"
  else
    echo "DELETE FROM \"${schema_migrations_table}\" WHERE \"${migration_id_column}\" = '$1';"
  fi
}


# Figure out which migrations to actually apply by comparin (using `comm`) the outputs from `applied_migrations` and `available_migrations`.
# 
# For migrations, use `"-13"` which means "Print only lines from file2 that are not present in file1."
# 
# For rollbacks, uss `"-12"` which means "Print only lines present in both file1 and file2."

# In[ ]:


function migrations_to_apply() {
  local comm_cols="-13"
  [[ $action == "rollback" ]] && comm_cols="-12"
  comm ${comm_cols} <(applied_migrations) <(available_migrations)
}


# Apply a given migration or rollback based on an ID.
# 
# Runs psql on the given .sql file for that ID, then runs post_apply_sql.
# 
# Example:
# 
# apply_migration_id 20200306033831
# 
# Migrating to 20200306033831
# done.

# In[ ]:


function apply_migration_id() {
  if [[ $action == "migrate" ]]; then
    echo -n Migrating to "$1..."
  else
    echo -n Rolling back "$1..."
  fi
  call_psql <<END
$(< "${migrations_dir}/$(available_migration_script_for_id "$1")")
$(post_apply_sql "$1") 
END
  local result=$?
  [[ $result -eq 0 ]] && echo done.
  return $result
}


# Use sed to stop once the version is found.
# 
# Example:
# 
#     truncate_migrations_if_requested "ghi" <<END
#     abc
#     def
#     ghi
#     jkl
#     END
# 
# Prints:
# 
#     abc
#     def
#     ghi
# 
# If the version is not found, will just return everything.

# In[ ]:


function truncate_migrations_if_requested() {
  if [[ -n $finish_at_version ]]; then
    sed -e "/^${finish_at_version}\$/q"
  else
    tee
  fi
}


# Putting it all together, this is the function that actually gets called via `migrator migrate` and `migrator rollback`.
# 
# Steps:
# * set `action` variable to either "migrate" or "rollback"
# * create the schema_migrations if it does not exist in postgres
# * Stop early if a `finish_at_version` argument is passed but no file matches it in the migrations directory.
# * Set `sort_dir` and `rolling_back` variables based on the `$action`
# * Get all the migrations to apply (by calling `migrations_to_apply`)
# * Sort the migrations to apply
# * Truncate up to the specified version if `finish_at_version` is supplied
# * Loop over these and call `apply_migration_id` on each one
# * Currently only allow rollbacks one-by-one

# In[ ]:


function migrate() {
  action=$1  # IMPORTANT: THIS IS SET AS A GLOBAL FOR USE IN OTHER FUNCTIONS
  call_psql <<END
  CREATE TABLE IF NOT EXISTS "${schema_migrations_table}" (
    "${migration_id_column}" VARCHAR(128) PRIMARY KEY NOT NULL
  );
END

  if [[ -n $finish_at_version ]] && ! migrations_to_apply | grep -q "^${finish_at_version}\$"; then
    echo -e "Migration ${finish_at_version} would not have been reached"
    exit 1
  fi

  local sort_dir=""
  local rolling_back="false"
  if [[ $action == "rollback" ]]; then
    sort_dir="-r"
    rolling_back="true"
  fi

  for migration_id in $(migrations_to_apply | sort ${sort_dir} | truncate_migrations_if_requested); do
    apply_migration_id "$migration_id"
    # Only roll back the most recent migration.
    # TODO: make rolling back number of migrations configurable
    $rolling_back && break
  done
}


# Switch to call the correct function for the given action.
# 
# `create-migration <name>` calls `touch` to generate filenames like `migrate-<generated date>-<name>.sql` and `rollback-<generated date>-<name>.sql`

# In[ ]:


case $action in
  remigrate)
    migrate rollback
    migrate migrate
    (";")
  migrate)
    migrate migrate
    (";")
  rollback)
    migrate rollback
    (";")
  create-migration)
    date=$(date +%Y%m%d%H%M%S)
    migration_name=$(echo "${migration_name}" | tr ' ' '-')
    upfile=${migrations_dir}/migrate-${date}-${migration_name}.sql;
    downfile=${migrations_dir}/rollback-${date}-${migration_name}.sql;
    touch "${upfile}" "${downfile}"
    echo "Created
          ${upfile}
          ${downfile}"
    (";")
  list)
    action="migrate"
    echo "available migration:"
    echo "$(available_migrations)"
    action="rollback"
    echo "available rollbacks:"
    echo "$(available_migrations)"
    echo "Applied migrations:"
    echo "$(applied_migrations)"
    (";")
esac

