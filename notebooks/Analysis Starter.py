#!/usr/bin/env python
# coding: utf-8

# In[7]:


get_ipython().run_line_magic('load_ext', 'watermark')
get_ipython().run_line_magic('watermark', '')
get_ipython().run_line_magic('matplotlib', 'notebook')
import matplotlib.pyplot as plt
import os
import sys
import json
import numpy as np
import pandas as pd
import seaborn as sns
import qgrid


# In[9]:


iris = sns.load_dataset('iris')
qgrid.show_grid(iris)


# In[15]:


plt.plot(iris.petal_length, iris.petal_width, '.')


# In[ ]:




